/**
 * 
 */
package com.ibm.marple;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Properties;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;

import org.apache.avro.Schema;
import org.apache.avro.file.DataFileReader;
import org.apache.avro.file.DataFileWriter;
import org.apache.avro.generic.GenericContainer;
import org.apache.avro.generic.GenericData;
import org.apache.avro.generic.GenericDatumReader;
import org.apache.avro.generic.GenericDatumWriter;
import org.apache.avro.generic.GenericRecord;
import org.apache.avro.io.DatumReader;
import org.apache.avro.io.DatumWriter;
import org.apache.avro.specific.SpecificDatumReader;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.TopicPartition;
import org.apache.kafka.common.errors.WakeupException;

import com.bbn.tc.schema.avro.TCCDMDatum;
import com.bbn.tc.schema.serialization.AvroConfig;
import com.google.gson.Gson;

/**
 * @author habeck
 * 
 * @copyright IBM Corporation 2106.  All rights reserved.
 *
 */
public class Deserializer {

	private long seed= System.currentTimeMillis();
	private Random generator;
	private boolean isKafkaSource=false;
	private boolean isJsonFile=false;
	private String topic=null;
	private String groupId;
	private final AtomicBoolean closed = new AtomicBoolean(false);

	private static boolean randomGroup = false;
	private static String defaultCDMFile = "data/livetest_1.bin";
	
	private static Properties g_props = new Properties();
	
	public  Long getNextRandom()
	{
		if (generator==null)
		{
			generator = new Random(seed);
		}
		return generator.nextLong();
	}
	
	
	public void setGroupId(String groupid)
	{
		groupId = groupid;
	}
	
	
	
	
    private static Properties createConsumerProps( String a_groupId ) {
        Properties props = new Properties();
        //props.put("zookeeper.connect", a_zookeeper);
        String bs = g_props.getProperty("bootstrap.servers","localhost:9092");
        //Log.info("boostrap.servers="+bs);
        props.put("bootstrap.servers", bs);
        props.put("group.id", a_groupId);
        //props.put("zookeeper.session.timeout.ms", "400");
        //props.put("zookeeper.sync.time.ms", "200");
        props.put("session.timeout.ms", "30000");
        props.put("auto.commit.interval.ms", "1000");

        props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG,"org.apache.kafka.common.serialization.StringDeserializer");
        props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, com.bbn.tc.schema.serialization.kafka.KafkaAvroGenericDeserializer.class);
        props.put(AvroConfig.SCHEMA_READER_FILE, "schema/TCCDMDatum.avsc");
        props.put(AvroConfig.SCHEMA_WRITER_FILE, "schema/TCCDMDatum.avsc");
        props.put(AvroConfig.SCHEMA_SERDE_IS_SPECIFIC,true);
        // --from-beginning
	    //props.put(org.apache.kafka.clients.consumer.ConsumerConfig.AUTO_OFFSET_RESET_CONFIG,"smallest");
	    props.put(org.apache.kafka.clients.consumer.ConsumerConfig.AUTO_OFFSET_RESET_CONFIG,"earliest");
 
        return props;
    }

	
	public  void processLabeledGraph_ta3()
	{
		Schema schema;
		Schema outputSchema;
		long processed = 0;
		long st = System.currentTimeMillis();
		try {
			schema = new Schema.Parser().parse(new File("/home/habeck/avro/schema/bitcoin.avsc"));
			outputSchema = new Schema.Parser().parse(new File("/home/habeck/avro/schema/bitcoin_s.avsc"));
			DatumReader<GenericRecord> datumReader = new GenericDatumReader<GenericRecord>(schema);
			File file = new File("/mnt/fiber/bitcoin1/habeck/benchmarks/src_data/avro_test/ian/bitcoin.avro");
			DataFileReader<GenericRecord> dataFileReader = new DataFileReader<GenericRecord>(file,datumReader);
			DataFileWriter<GenericRecord> dataFileWriter = openOF("/mnt/fiber/bitcoin1/habeck/benchmarks/avro/bitcoin_s.avro",outputSchema);
			GenericRecord record = null;
			while (dataFileReader.hasNext())
			{
				record = dataFileReader.next(record);

				long fromId = (Long) record.get("fromaccount");
				long toId = (Long)record.get("toaccount");
				long amount = (Long)record.get("amount");
				//Long amount = getNextRandom();
				GenericRecord outputRec = new GenericData.Record(outputSchema);
				outputRec.put("fromaccount", String.valueOf(fromId));
				outputRec.put("toaccount",String.valueOf(toId));
				outputRec.put("amount",String.valueOf(amount));
				dataFileWriter.append(outputRec);
				processed+=1;
				if (processed%100000==0)
				{
					long et = System.currentTimeMillis();
					System.out.println("Processed "+processed+" in "+((et-st)/1000)+"s");
				}
			}
			dataFileWriter.close();
			dataFileReader.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public  DataFileWriter<GenericRecord> openOF(String outputFilePath,Schema schema) throws IOException
	{
		File file  = new File(outputFilePath);
		DatumWriter<GenericRecord> datumWriter = new GenericDatumWriter<GenericRecord>(schema);
		DataFileWriter<GenericRecord> dataFileWriter = new DataFileWriter<GenericRecord>(datumWriter);
		dataFileWriter.create(schema,file);
		return dataFileWriter; 
	}
	
	public  void processLabeledGraph_bitcoin_ta2()
	{
		Schema schema;
		try {
			schema = new Schema.Parser().parse(new File("/home/habeck/avro/schema/bitcoin.avsc"));
			DatumReader<GenericRecord> datumReader = new GenericDatumReader<GenericRecord>(schema);
			File file = new File("/home/habeck/avro/src_data/bitcoin.avro");
			DataFileReader<GenericRecord> dataFileReader = new DataFileReader<GenericRecord>(file,datumReader);
			GenericRecord record = null;
			while (dataFileReader.hasNext())
			{
				record = dataFileReader.next(record);
				long from  = (Long)record.get("fromaccount");
				long to    = (Long)record.get("toaccount");
				long amount= (Long)record.get("amount");
				//Record fields = (Record)record.get("fields");
				System.out.println("fromaccount: "+from);
				System.out.println("toaccount: "+to);
				System.out.println("amount:"+amount);
			}
			dataFileReader.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	private void processJsonFile(String jsonFile) throws IOException {
		int count=0;
		Neo4JProcessor neo4j= new Neo4JProcessor(g_props);
		BufferedReader br = new BufferedReader(new FileReader(new File(jsonFile)));
		
		String json= null;
        while (( json = br.readLine()) != null)
        {
		    neo4j.store(json);
		    count+=1;
        }
		if (neo4j!=null) {
		   neo4j.shutdown();
		System.out.println("Processed "+count+" JSON records.");
	}
	}

	
	public void processCDMData(String cdmFile) {
		// /Users/habeck/zOS/MarpleCDM/resources/avro/CDM10.avdl
		int count=0;
		Neo4JProcessor neo4j=null;
	      try {
	  		Schema schema = new Schema.Parser().parse(new File("schema/TCCDMDatum.avsc"));
			DatumReader<TCCDMDatum> datumReader = new SpecificDatumReader<TCCDMDatum>(schema);
		    DataFileReader<TCCDMDatum> dataFileReader = null;
			dataFileReader = new DataFileReader(new File(cdmFile), datumReader);
			TCCDMDatum transaction = null;
			neo4j = new Neo4JProcessor(g_props);
		     while (dataFileReader.hasNext()) {
		    	 transaction = dataFileReader.next(transaction);
		         System.out.println(transaction);
		         neo4j.store(transaction);
		         count+=1;
		         if (count>=680)
		         {
		        	  boolean breakHere=true;
		        	  System.out.println("OK debug it.");
		         }
		     }
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if (neo4j!=null) {
			   neo4j.shutdown();
			}
			System.out.println("Processed "+count+" CDM records.");
		}
	}
	
	
	public void runKafkaConsumer()
	{
	    KafkaConsumer consumer = new KafkaConsumer(createConsumerProps(groupId));
	    Neo4JProcessor acp=null;
	    long beginning = System.currentTimeMillis();
		try {
			Schema schema = new Schema.Parser().parse(new File("schema/TCCDMDatum.avsc"));
			DatumReader<TCCDMDatum> datumReader = new SpecificDatumReader<TCCDMDatum>(schema);
            TopicPartition partition0 = new TopicPartition(topic, 0);
            consumer.assign(Arrays.asList(partition0));
            //consumer.subscribe(Arrays.asList(topic));
            //Log.info("Seeking to the beginning of the topic.");
            Set<TopicPartition> partitions = consumer.assignment();
            for (TopicPartition pInfo : partitions)
            {
            	//Log.info("seeking to beginning of partition "+pInfo.partition()+".");
            	consumer.seekToBeginning(pInfo);            	
            }
            boolean exiton0=Boolean.valueOf(g_props.getProperty("exiton0"));
			acp = new Neo4JProcessor(g_props);
			acp.setTopic(topic);
            while (!closed.get()) {
            	//Log.info("Polling...");
                long now = System.currentTimeMillis();
                long start = now;
                long elapsed =  (now - beginning )/1000;
            	System.out.println("["+elapsed+"] Polling...");
                @SuppressWarnings("unchecked")
				ConsumerRecords<String, GenericContainer> records = consumer.poll(10000);
                //Log.info("received: "+records.count()+" messages...");
                // Handle new records
                now = System.currentTimeMillis();
                elapsed =  (now - beginning )/1000;
                long interval = (now - start)/1000;
                System.out.println("["+elapsed+"]["+interval+"] received: "+records.count()+" messages...");
                Iterator recIter=records.iterator();
                while (recIter.hasNext())
                {
                	ConsumerRecord<String, GenericContainer> record = (ConsumerRecord<String, GenericContainer>)recIter.next();
                	GenericContainer ctr = record.value();
                	TCCDMDatum datum = (TCCDMDatum) record.value();
                	acp.store(datum);
                }
                if (records.count()>0) {
                   acp.shutdown();
                   acp.open();
                } else {
                	if (exiton0)
                	{
                		closed.set(true);
                	}
                }
            }

            
		} catch (WakeupException e) {
			// Ignore exception if closing
			if (!closed.get())
				throw e;
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			consumer.close();
			if (acp!=null)
			{
			  acp.shutdown();
			}
//			if (neo4j != null) {
//				neo4j.shutdown();
//			}
		}
	}
	
	
	private void printSyntax() {
		System.out.println("(C) IBM Corporation 2016. All Rights Reserved.\n");
		System.out.println("Syntax: java -jar Neo4JCdmConsumer.jar  [ -topic <topic_name> | -file <avro CDM filename> | -json <avro_json>] -dbfolder <folder_name_of_neo4j_db> -cfgfolder <folder_name_of_config_folder>");
		System.out.println("\nThe -topic argument takes precidence over the -file argument.");
	}
	
	private void parseArgs(String args[]) throws FileNotFoundException, IOException
	{
		boolean isPropertyFileLoaded=false;
		Properties overrides = new Properties();
	   if (args.length>0)
	   {
		   for(int i=0;i<args.length;i++)
		   {
			      //System.out.println("arg["+i+"]:"+args[i]);
			      if ("-topic".equals(args[i]))
			      {
				    	  if (args.length>(i+1))
				    	  {
				    	     topic = args[i+1];
				    	     isKafkaSource=true;
				    		  i+=1;
				    	  } else {
				    		  System.err.println("No topic parameter supplied.");;
				    		  printSyntax();
				    		  System.exit(1);
				    	  }
			      } else if ("-h".equals(args[i])) {
				    	  printSyntax();
				    	  System.exit(0);
			      } else if ("-file".equals(args[i]))
			      {
				    	  if (args.length>(i+1))
				    	  {
				    		  defaultCDMFile = args[i+1];
				    		  i+=1;
				    	  }			    	  
			      } else if ("-json".equals(args[i]))
			      {
				    	  if (args.length>(i+1))
				    	  {
				    		  defaultCDMFile = args[i+1];
				    		  isJsonFile=true;
				    		  i+=1;
				    	  }			    	  
			    	      
			      } else if ("-dbfolder".equals(args[i]))
			      {
				    	  if (args.length>(i+1))
				    	  {
				    		  overrides.setProperty("DEFAULT_STORE_FOLDER",args[i+1]);
				    		  i+=1;
				    	  }			    	   
			      } else if ("-cfgfolder".equals(args[i]))
			      {
				    	  if (args.length>(i+1))
				    	  {
				    		  //g_props.setProperty("DEFAULT_STORE_FOLDER",args[i+1]);
				    		String cfgFolder = args[i+1];
				    		if (!cfgFolder.endsWith(File.separator)) { cfgFolder+=File.separator;}
						g_props.load(new FileInputStream(new File(cfgFolder+"Deserializer.properties")));
						g_props.load(new FileInputStream(new File(cfgFolder+"BatchImporter.properties")));
			    		    isPropertyFileLoaded=true;
						i+=1;
				    	  }
			      }
		      }
		      if (!isPropertyFileLoaded) 
		      {
					g_props.load(new FileInputStream(new File("conf/Deserializer.properties")));
					g_props.load(new FileInputStream(new File("conf/BatchImporter.properties")));
		      }
		      g_props.putAll(overrides);
			  randomGroup = Boolean.valueOf(g_props.getProperty("randomGroup","False"));

		   }
	   }

	
	/**
	 * @param args
	 * @throws IOException 
	 */
	public static void main(String[] args) throws IOException {
		Deserializer ds = new Deserializer();
		defaultCDMFile = g_props.getProperty("cdmfile",defaultCDMFile);
		ds.parseArgs(args);
		//ds.processLabeledGraph_ta3();
		if (!ds.isKafkaSource)
		{
			if (!ds.isJsonFile)
			{
				ds.processCDMData(defaultCDMFile);				
			} else {
				ds.processJsonFile(defaultCDMFile);
			}
		} else {
			  if (Deserializer.randomGroup)
			  {
			    ds.setGroupId("MARPLE"+String.valueOf(ds.getNextRandom()));
			  } else {
				ds.setGroupId("MARPLE");  
			  }
			  ds.runKafkaConsumer();	

		}
	}



}
