/**
 * 
 */
package com.ibm.marple;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.TreeMap;

import javax.xml.bind.DatatypeConverter;

import org.apache.avro.Schema;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.neo4j.collection.primitive.base.Empty.EmptyPrimitiveLongCollection;
import org.neo4j.graphdb.Label;
import org.neo4j.graphdb.RelationshipType;
import org.neo4j.unsafe.batchinsert.BatchInserter;
import org.neo4j.unsafe.batchinsert.BatchInserters;

import com.bbn.tc.schema.avro.ConfidentialityTag;
import com.bbn.tc.schema.avro.EdgeType;
import com.bbn.tc.schema.avro.Event;
import com.bbn.tc.schema.avro.EventType;
import com.bbn.tc.schema.avro.FileObject;
import com.bbn.tc.schema.avro.IntegrityTag;
import com.bbn.tc.schema.avro.MemoryObject;
import com.bbn.tc.schema.avro.NetFlowObject;
import com.bbn.tc.schema.avro.Principal;
import com.bbn.tc.schema.avro.ProvenanceTagNode;
import com.bbn.tc.schema.avro.RegistryKeyObject;
import com.bbn.tc.schema.avro.SHORT;
import com.bbn.tc.schema.avro.SimpleEdge;
import com.bbn.tc.schema.avro.SrcSinkObject;
import com.bbn.tc.schema.avro.Subject;
import com.bbn.tc.schema.avro.SubjectType;
import com.bbn.tc.schema.avro.TCCDMDatum;
import com.bbn.tc.schema.avro.TagEntity;
import com.bbn.tc.schema.avro.TagOpCode;
import com.bbn.tc.schema.avro.UUID;
import com.bbn.tc.schema.avro.Value;
import com.bbn.tc.schema.avro.ValueDataType;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * @author habeck
 * (C) IBM Corporation 2016 All Rights Reserved
 */
public class Neo4JProcessor {
	
	private HashMap<IpPortObject, Long> netflowVertexMap = new HashMap<IpPortObject, Long>(); 
	private HashMap<String, Long> unknowObjectVertexMap = new HashMap<String, Long>();
	private HashMap<Long, ArrayList<Long>> eventVertex2Parms = new HashMap<Long, ArrayList<Long>>();
	private HashMap<Integer, ArrayList<Long>> pid2pidVertex = new HashMap<Integer, ArrayList<Long>>();
	private HashMap<Integer, ArrayList<Integer>> ppid2pid = new HashMap<Integer, ArrayList<Integer>>();
	
	  TreeMap<String,Object> nodeIds = new TreeMap<String,Object>();
	  private Map<CUUID,Long> vertexMap = new TreeMap<CUUID,Long>();
	  private Map<Long,CUUID> rVertexMap = new TreeMap<Long,CUUID>();
	  private Properties g_props;
	  BatchInserter bi;
	  long beginning= System.currentTimeMillis();
	  boolean debug = false;
	public enum MarpleLabels implements Label {
		Principal, Subject, Event, NetflowObject, FileObject,SrcSinkObject,ProvenanceTagNode,MemoryObject,TagEntity,RegistryKeyObject,UnknowObject,
		EVENT_ACCEPT, EVENT_BIND, EVENT_CHANGE_PRINCIPAL, EVENT_CHECK_FILE_ATTRIBUTES, EVENT_CLONE, EVENT_CLOSE, 
		EVENT_CONNECT, EVENT_CREATE_OBJECT, EVENT_CREATE_THREAD, EVENT_EXECUTE, EVENT_FORK, EVENT_LINK, EVENT_UNLINK, 
		EVENT_MMAP, EVENT_MODIFY_FILE_ATTRIBUTES, EVENT_MPROTECT, EVENT_OPEN, EVENT_READ, EVENT_RECVFROM, EVENT_RECVMSG, 
		EVENT_RENAME, EVENT_WRITE, EVENT_SIGNAL, EVENT_TRUNCATE, EVENT_WAIT, EVENT_OS_UNKNOWN, EVENT_KERNEL_UNKNOWN, 
		EVENT_APP_UNKNOWN, EVENT_UI_UNKNOWN, EVENT_UNKNOWN, EVENT_BLIND, EVENT_UNIT, EVENT_UPDATE, EVENT_SENDTO, EVENT_SENDMSG, 
		EVENT_SHM, EVENT_EXIT, 
		SUBJECT_PROCESS,SUBJECT_THREAD,SUBJECT_UNIT,SUBJECT_BASIC_BLOCK		 
	}
	
		
	public enum MarpleRelationships implements RelationshipType
	{
		EDGE_EVENT_AFFECTS_MEMORY, EDGE_EVENT_AFFECTS_FILE, EDGE_EVENT_AFFECTS_NETFLOW, EDGE_EVENT_AFFECTS_SUBJECT, EDGE_EVENT_AFFECTS_SRCSINK, EDGE_EVENT_HASPARENT_EVENT, EDGE_EVENT_CAUSES_EVENT, EDGE_EVENT_ISGENERATEDBY_SUBJECT, EDGE_SUBJECT_AFFECTS_EVENT, EDGE_SUBJECT_HASPARENT_SUBJECT, EDGE_SUBJECT_HASLOCALPRINCIPAL, EDGE_SUBJECT_RUNSON, EDGE_FILE_AFFECTS_EVENT, EDGE_NETFLOW_AFFECTS_EVENT, EDGE_MEMORY_AFFECTS_EVENT, EDGE_SRCSINK_AFFECTS_EVENT, EDGE_OBJECT_PREV_VERSION, EDGE_FILE_HAS_TAG, EDGE_NETFLOW_HAS_TAG, EDGE_MEMORY_HAS_TAG, EDGE_SRCSINK_HAS_TAG, EDGE_SUBJECT_HAS_TAG, EDGE_EVENT_HAS_TAG, EDGE_EVENT_AFFECTS_REGISTRYKEY, EDGE_REGISTRYKEY_AFFECTS_EVENT, EDGE_REGISTRYKEY_HAS_TAG, EDGE_EVENT_AFFECTS_OBJECT,EDGE_OBJECT_ISGENERATEDBY_SUBJECT  
	}
	  /*
	   private static final Label PRINCIPAL_LABEL = DynamicLabel.label("Principal");
	   private static final Label SUBJECT_LABEL = DynamicLabel.label("Subject");
	   private static final Label EVENT_LABEL = DynamicLabel.label("Event");
	   private static final Label NETFLOW_LABEL = DynamicLabel.label("NetFlowObject");
	   private static final Label FILEOBJECT_LABEL = DynamicLabel.label("FileObject");
	   
	   private static final Label EVENT_ACCEPT = DynamicLabel.label("EVENT_ACCEPT");
	   private static final Label EVENT_BIND = DynamicLabel.label("EVENT_BIND");
	   private static final Label EVENT_CHANGE_PRINCIPAL = DynamicLabel.label("EVENT_CHANGE_PRINCIPAL");
	   private static final Label EVENT_CHECK_FILE_ATTRIBUTES = DynamicLabel.label("EVENT_CHECK_FILE_ATTRIBUTES");
	   private static final Label EVENT_CLONE = DynamicLabel.label("EVENT_CLONE");
	   private static final Label EVENT_CLOSE = DynamicLabel.label("EVENT_CLOSE");
	   private static final Label EVENT_CONNECT= DynamicLabel.label("EVENT_CONNECT");
	   private static final Label EVENT_CREATE_OBJECT= DynamicLabel.label("EVENT_CREATE_OBJECT");
	   private static final Label EVENT_CREATE_THREAD= DynamicLabel.label("EVENT_CREATE_THREAD");
	   private static final Label EVENT_EXECUTE= DynamicLabel.label("EVENT_EXECUTE");
	   private static final Label EVENT_FORK= DynamicLabel.label("EVENT_FORK");
	   private static final Label EVENT_LINK= DynamicLabel.label("EVENT_LINK");
	   private static final Label EVENT_UNLINK= DynamicLabel.label("EVENT_UNLINK");
	   private static final Label EVENT_MMAP= DynamicLabel.label("EVENT_MMAP");
	   private static final Label EVENT_MODIFY_FILE_ATTRIBUTES= DynamicLabel.label("EVENT_MODIFY_FILE_ATTRIBUTES");
	   private static final Label EVENT_MPROTECT= DynamicLabel.label("EVENT_MPROTECT");
	   private static final Label EVENT_OPEN= DynamicLabel.label("EVENT_OPEN");
	   private static final Label EVENT_READ= DynamicLabel.label("EVENT_READ");
	   private static final Label EVENT_RECVFROM= DynamicLabel.label("EVENT_RECVFROM");
	   private static final Label EVENT_RECVMSG= DynamicLabel.label("EVENT_RECVMSG");
	   private static final Label EVENT_RENAME= DynamicLabel.label("EVENT_RENAME");
	   private static final Label EVENT_WRITE= DynamicLabel.label("EVENT_WRITE");
	   private static final Label EVENT_SIGNAL= DynamicLabel.label("EVENT_SIGNAL");
	   private static final Label EVENT_TRUNCATE= DynamicLabel.label("EVENT_TRUNCATE");
	   private static final Label EVENT_WAIT= DynamicLabel.label("EVENT_WAIT");
	   private static final Label EVENT_OS_UNKNOWN= DynamicLabel.label("EVENT_OS_UNKNOWN");
	   private static final Label EVENT_KERNEL_UNKNOWN= DynamicLabel.label("EVENT_KERNEL_UNKNOWN");
	   private static final Label EVENT_APP_UNKNOWN= DynamicLabel.label("EVENT_APP_UNKNOWN");
	   private static final Label EVENT_UI_UNKNOWN= DynamicLabel.label("EVENT_UI_UNKNOWN");
	   private static final Label EVENT_UNKNOWN= DynamicLabel.label("EVENT_UNKNOWN");
	   private static final Label EVENT_BLIND= DynamicLabel.label("EVENT_BLIND");
	   private static final Label EVENT_UNIT= DynamicLabel.label("EVENT_UNIT");
	   private static final Label EVENT_UPDATE= DynamicLabel.label("EVENT_UPDATE");
	   
	   
	   private static final Label SUBJECT_PROCESS = DynamicLabel.label("SUBJECT_PROCESS");
	   private static final Label SUBJECT_THREAD= DynamicLabel.label("SUBJECT_THREAD");
	   private static final Label SUBJECT_UNIT= DynamicLabel.label("SUBJECT_UNIT");
	   private static final Label SUBJECT_BASIC_BLOCK= DynamicLabel.label("SUBJECT_BASIC_BLOCK");
	   */
	   private static HashMap< EventType,Label> eventMap = new HashMap<EventType, Label>();
	   private static HashMap< String,Label> sEventMap = new HashMap<String, Label>();
	   
	   private static HashMap<SubjectType,Label> subjectMap = new HashMap<SubjectType,Label>();
	   
	   private static HashMap<String,Label> sSubjectMap = new HashMap<String, Label>();
	   
	   private static HashMap<String,RelationshipType> sEdgeMap = new HashMap<String,RelationshipType>();
	   
	   private static HashMap<EdgeType,RelationshipType> edgeMap = new HashMap<EdgeType,RelationshipType>();
	   
	   private Gson gson = new Gson();
	   private String topic;
      // EventType.EVENT_CLONE;
	  /**
	   * Used for message logging to console or file.  
	   */
	  private static final Log log = LogFactory.getLog(Neo4JProcessor.class);

	  
	  /**
	   * Static constructor
	   */
	  {
	    //g_props = new Properties();
	      //g_props.load(new FileInputStream(new File("conf/BatchImporter.properties")));
	      eventMap.put(EventType.EVENT_ACCEPT,MarpleLabels.EVENT_ACCEPT);
	      eventMap.put(EventType.EVENT_APP_UNKNOWN,MarpleLabels.EVENT_APP_UNKNOWN);
	      eventMap.put(EventType.EVENT_BIND, MarpleLabels.EVENT_BIND);
	      eventMap.put(EventType.EVENT_BLIND,MarpleLabels.EVENT_BLIND);
	      eventMap.put(EventType.EVENT_CHANGE_PRINCIPAL,MarpleLabels.EVENT_CHANGE_PRINCIPAL);
	      eventMap.put(EventType.EVENT_CHECK_FILE_ATTRIBUTES,MarpleLabels.EVENT_CHECK_FILE_ATTRIBUTES);
	      eventMap.put(EventType.EVENT_CLONE,MarpleLabels.EVENT_CLONE);
	      eventMap.put(EventType.EVENT_CLOSE,MarpleLabels.EVENT_CLOSE);
	      eventMap.put(EventType.EVENT_CONNECT,MarpleLabels.EVENT_CONNECT);
	      eventMap.put(EventType.EVENT_CREATE_OBJECT,MarpleLabels.EVENT_CREATE_OBJECT);
	      eventMap.put(EventType.EVENT_CREATE_THREAD,MarpleLabels.EVENT_CREATE_THREAD);
	      eventMap.put(EventType.EVENT_EXIT,MarpleLabels.EVENT_EXIT);
	      eventMap.put(EventType.EVENT_EXECUTE,MarpleLabels.EVENT_EXECUTE);
	      eventMap.put(EventType.EVENT_FORK,MarpleLabels.EVENT_FORK);
	      eventMap.put(EventType.EVENT_KERNEL_UNKNOWN,MarpleLabels.EVENT_KERNEL_UNKNOWN);
	      eventMap.put(EventType.EVENT_LINK,MarpleLabels.EVENT_LINK);
	      eventMap.put(EventType.EVENT_MMAP,MarpleLabels.EVENT_MMAP);
	      eventMap.put(EventType.EVENT_MODIFY_FILE_ATTRIBUTES,MarpleLabels.EVENT_MODIFY_FILE_ATTRIBUTES);
	      eventMap.put(EventType.EVENT_MPROTECT,MarpleLabels.EVENT_MPROTECT);
	      eventMap.put(EventType.EVENT_OPEN,MarpleLabels.EVENT_OPEN);
	      eventMap.put(EventType.EVENT_OS_UNKNOWN,MarpleLabels.EVENT_OS_UNKNOWN);
	      eventMap.put(EventType.EVENT_READ,MarpleLabels.EVENT_READ);
	      eventMap.put(EventType.EVENT_RECVFROM,MarpleLabels.EVENT_RECVFROM );
	      eventMap.put(EventType.EVENT_RECVMSG, MarpleLabels.EVENT_RECVMSG);
	      eventMap.put(EventType.EVENT_RENAME, MarpleLabels.EVENT_RENAME);
	      eventMap.put(EventType.EVENT_SIGNAL, MarpleLabels.EVENT_SIGNAL);
	      eventMap.put(EventType.EVENT_SENDTO, MarpleLabels.EVENT_SENDTO);
	      eventMap.put(EventType.EVENT_SHM, MarpleLabels.EVENT_SHM);
	      eventMap.put(EventType.EVENT_SENDMSG, MarpleLabels.EVENT_SENDMSG);
	      
	      eventMap.put(EventType.EVENT_TRUNCATE, MarpleLabels.EVENT_TRUNCATE);
	      eventMap.put(EventType.EVENT_UI_UNKNOWN,MarpleLabels.EVENT_UI_UNKNOWN);
	      eventMap.put(EventType.EVENT_UNIT,MarpleLabels.EVENT_UNIT);
	      eventMap.put(EventType.EVENT_UNKNOWN, MarpleLabels.EVENT_UNKNOWN);
	      eventMap.put(EventType.EVENT_UNLINK, MarpleLabels.EVENT_UNLINK);
	      eventMap.put(EventType.EVENT_UPDATE,MarpleLabels.EVENT_UPDATE );
	      eventMap.put(EventType.EVENT_WAIT, MarpleLabels.EVENT_WAIT);
	      eventMap.put(EventType.EVENT_WRITE,MarpleLabels.EVENT_WRITE);

	      sEventMap.put(EventType.EVENT_ACCEPT.toString(),MarpleLabels.EVENT_ACCEPT);
	      sEventMap.put(EventType.EVENT_APP_UNKNOWN.toString(),MarpleLabels.EVENT_APP_UNKNOWN);
	      sEventMap.put(EventType.EVENT_BIND.toString(), MarpleLabels.EVENT_BIND);
	      sEventMap.put(EventType.EVENT_BLIND.toString(),MarpleLabels.EVENT_BLIND);
	      sEventMap.put(EventType.EVENT_CHANGE_PRINCIPAL.toString(),MarpleLabels.EVENT_CHANGE_PRINCIPAL);
	      sEventMap.put(EventType.EVENT_CHECK_FILE_ATTRIBUTES.toString(),MarpleLabels.EVENT_CHECK_FILE_ATTRIBUTES);
	      sEventMap.put(EventType.EVENT_CLONE.toString(),MarpleLabels.EVENT_CLONE);
	      sEventMap.put(EventType.EVENT_CLOSE.toString(),MarpleLabels.EVENT_CLOSE);
	      sEventMap.put(EventType.EVENT_CONNECT.toString(),MarpleLabels.EVENT_CONNECT);
	      sEventMap.put(EventType.EVENT_CREATE_OBJECT.toString(),MarpleLabels.EVENT_CREATE_OBJECT);
	      sEventMap.put(EventType.EVENT_CREATE_THREAD.toString(),MarpleLabels.EVENT_CREATE_THREAD);
	      sEventMap.put(EventType.EVENT_EXECUTE.toString(),MarpleLabels.EVENT_EXECUTE);
	      sEventMap.put(EventType.EVENT_EXIT.toString(),MarpleLabels.EVENT_EXIT);
	      sEventMap.put(EventType.EVENT_FORK.toString(),MarpleLabels.EVENT_FORK);
	      sEventMap.put(EventType.EVENT_KERNEL_UNKNOWN.toString(),MarpleLabels.EVENT_KERNEL_UNKNOWN);
	      sEventMap.put(EventType.EVENT_LINK.toString(),MarpleLabels.EVENT_LINK);
	      sEventMap.put(EventType.EVENT_MMAP.toString(),MarpleLabels.EVENT_MMAP);
	      sEventMap.put(EventType.EVENT_MODIFY_FILE_ATTRIBUTES.toString(),MarpleLabels.EVENT_MODIFY_FILE_ATTRIBUTES);
	      sEventMap.put(EventType.EVENT_MPROTECT.toString(),MarpleLabels.EVENT_MPROTECT);
	      sEventMap.put(EventType.EVENT_OPEN.toString(),MarpleLabels.EVENT_OPEN);
	      sEventMap.put(EventType.EVENT_OS_UNKNOWN.toString(),MarpleLabels.EVENT_OS_UNKNOWN);
	      sEventMap.put(EventType.EVENT_READ.toString(),MarpleLabels.EVENT_READ);
	      sEventMap.put(EventType.EVENT_RECVFROM.toString(),MarpleLabels.EVENT_RECVFROM );
	      sEventMap.put(EventType.EVENT_RECVMSG.toString(), MarpleLabels.EVENT_RECVMSG);
	      sEventMap.put(EventType.EVENT_RENAME.toString(), MarpleLabels.EVENT_RENAME);
	      sEventMap.put(EventType.EVENT_SIGNAL.toString(), MarpleLabels.EVENT_SIGNAL);
	      sEventMap.put(EventType.EVENT_SENDTO.toString(), MarpleLabels.EVENT_SENDTO);
	      sEventMap.put(EventType.EVENT_SHM.toString(), MarpleLabels.EVENT_SHM);
	      sEventMap.put(EventType.EVENT_SENDMSG.toString(), MarpleLabels.EVENT_SENDMSG);
	      sEventMap.put(EventType.EVENT_TRUNCATE.toString(), MarpleLabels.EVENT_TRUNCATE);
	      sEventMap.put(EventType.EVENT_UI_UNKNOWN.toString(),MarpleLabels.EVENT_UI_UNKNOWN);
	      sEventMap.put(EventType.EVENT_UNIT.toString(),MarpleLabels.EVENT_UNIT);
	      sEventMap.put(EventType.EVENT_UNKNOWN.toString(), MarpleLabels.EVENT_UNKNOWN);
	      sEventMap.put(EventType.EVENT_UNLINK.toString(), MarpleLabels.EVENT_UNLINK);
	      sEventMap.put(EventType.EVENT_UPDATE.toString(),MarpleLabels.EVENT_UPDATE );
	      sEventMap.put(EventType.EVENT_WAIT.toString(), MarpleLabels.EVENT_WAIT);
	      sEventMap.put(EventType.EVENT_WRITE.toString(),MarpleLabels.EVENT_WRITE);


	      
	      subjectMap.put(SubjectType.SUBJECT_BASIC_BLOCK,MarpleLabels.SUBJECT_BASIC_BLOCK);
	      subjectMap.put(SubjectType.SUBJECT_PROCESS,MarpleLabels.SUBJECT_PROCESS);
	      subjectMap.put(SubjectType.SUBJECT_THREAD,MarpleLabels.SUBJECT_THREAD);
	      subjectMap.put(SubjectType.SUBJECT_UNIT,MarpleLabels.SUBJECT_UNIT);
	      sSubjectMap.put(SubjectType.SUBJECT_BASIC_BLOCK.toString(),MarpleLabels.SUBJECT_BASIC_BLOCK);
	      sSubjectMap.put(SubjectType.SUBJECT_PROCESS.toString(),MarpleLabels.SUBJECT_PROCESS);
	      sSubjectMap.put(SubjectType.SUBJECT_THREAD.toString(),MarpleLabels.SUBJECT_THREAD);
	      sSubjectMap.put(SubjectType.SUBJECT_UNIT.toString(),MarpleLabels.SUBJECT_UNIT);
	      
	      
	      edgeMap.put(EdgeType.EDGE_EVENT_AFFECTS_FILE,MarpleRelationships.EDGE_EVENT_AFFECTS_FILE);
	      edgeMap.put(EdgeType.EDGE_EVENT_AFFECTS_MEMORY,MarpleRelationships.EDGE_EVENT_AFFECTS_MEMORY);
	      edgeMap.put(EdgeType.EDGE_EVENT_AFFECTS_NETFLOW,MarpleRelationships.EDGE_EVENT_AFFECTS_NETFLOW);
	      edgeMap.put(EdgeType.EDGE_EVENT_AFFECTS_SRCSINK,MarpleRelationships.EDGE_EVENT_AFFECTS_SRCSINK);
	      edgeMap.put(EdgeType.EDGE_EVENT_AFFECTS_SUBJECT,MarpleRelationships.EDGE_EVENT_AFFECTS_SUBJECT);
	      edgeMap.put(EdgeType.EDGE_EVENT_CAUSES_EVENT,MarpleRelationships.EDGE_EVENT_CAUSES_EVENT);
	      edgeMap.put(EdgeType.EDGE_EVENT_HAS_TAG,MarpleRelationships.EDGE_EVENT_HAS_TAG);
	      edgeMap.put(EdgeType.EDGE_EVENT_HASPARENT_EVENT,MarpleRelationships.EDGE_EVENT_HASPARENT_EVENT);
	      edgeMap.put(EdgeType.EDGE_EVENT_ISGENERATEDBY_SUBJECT,MarpleRelationships.EDGE_EVENT_ISGENERATEDBY_SUBJECT);
	      edgeMap.put(EdgeType.EDGE_FILE_AFFECTS_EVENT,MarpleRelationships.EDGE_FILE_AFFECTS_EVENT);
	      edgeMap.put(EdgeType.EDGE_FILE_HAS_TAG,MarpleRelationships.EDGE_FILE_HAS_TAG);
	      edgeMap.put(EdgeType.EDGE_MEMORY_AFFECTS_EVENT,MarpleRelationships.EDGE_MEMORY_AFFECTS_EVENT);
	      edgeMap.put(EdgeType.EDGE_NETFLOW_HAS_TAG,MarpleRelationships.EDGE_NETFLOW_HAS_TAG);
	      edgeMap.put(EdgeType.EDGE_OBJECT_PREV_VERSION,MarpleRelationships.EDGE_OBJECT_PREV_VERSION);
	      edgeMap.put(EdgeType.EDGE_SRCSINK_AFFECTS_EVENT,MarpleRelationships.EDGE_SRCSINK_AFFECTS_EVENT);
	      edgeMap.put(EdgeType.EDGE_SRCSINK_HAS_TAG,MarpleRelationships.EDGE_SRCSINK_HAS_TAG);
	      edgeMap.put(EdgeType.EDGE_SUBJECT_AFFECTS_EVENT,MarpleRelationships.EDGE_SUBJECT_AFFECTS_EVENT);
	      edgeMap.put(EdgeType.EDGE_SUBJECT_HAS_TAG,MarpleRelationships.EDGE_SUBJECT_HAS_TAG);
	      edgeMap.put(EdgeType.EDGE_SUBJECT_HASLOCALPRINCIPAL,MarpleRelationships.EDGE_SUBJECT_HASLOCALPRINCIPAL);
	      edgeMap.put(EdgeType.EDGE_SUBJECT_HASPARENT_SUBJECT,MarpleRelationships.EDGE_SUBJECT_HASPARENT_SUBJECT);
	      edgeMap.put(EdgeType.EDGE_SUBJECT_RUNSON,MarpleRelationships.EDGE_SUBJECT_RUNSON);
	      edgeMap.put(EdgeType.EDGE_NETFLOW_AFFECTS_EVENT,MarpleRelationships.EDGE_NETFLOW_AFFECTS_EVENT);
	      edgeMap.put(EdgeType.EDGE_MEMORY_HAS_TAG, MarpleRelationships.EDGE_MEMORY_HAS_TAG);

	      sEdgeMap.put(EdgeType.EDGE_EVENT_AFFECTS_FILE.toString(),MarpleRelationships.EDGE_EVENT_AFFECTS_FILE);
	      sEdgeMap.put(EdgeType.EDGE_EVENT_AFFECTS_MEMORY.toString(),MarpleRelationships.EDGE_EVENT_AFFECTS_MEMORY);
	      sEdgeMap.put(EdgeType.EDGE_EVENT_AFFECTS_NETFLOW.toString(),MarpleRelationships.EDGE_EVENT_AFFECTS_NETFLOW);
	      sEdgeMap.put(EdgeType.EDGE_EVENT_AFFECTS_SRCSINK.toString(),MarpleRelationships.EDGE_EVENT_AFFECTS_SRCSINK);
	      sEdgeMap.put(EdgeType.EDGE_EVENT_AFFECTS_SUBJECT.toString(),MarpleRelationships.EDGE_EVENT_AFFECTS_SUBJECT);
	      sEdgeMap.put(EdgeType.EDGE_EVENT_CAUSES_EVENT.toString(),MarpleRelationships.EDGE_EVENT_CAUSES_EVENT);
	      sEdgeMap.put(EdgeType.EDGE_EVENT_HAS_TAG.toString(),MarpleRelationships.EDGE_EVENT_HAS_TAG);
	      sEdgeMap.put(EdgeType.EDGE_EVENT_HASPARENT_EVENT.toString(),MarpleRelationships.EDGE_EVENT_HASPARENT_EVENT);
	      sEdgeMap.put(EdgeType.EDGE_EVENT_ISGENERATEDBY_SUBJECT.toString(),MarpleRelationships.EDGE_EVENT_ISGENERATEDBY_SUBJECT);
	      sEdgeMap.put(EdgeType.EDGE_FILE_AFFECTS_EVENT.toString(),MarpleRelationships.EDGE_FILE_AFFECTS_EVENT);
	      sEdgeMap.put(EdgeType.EDGE_FILE_HAS_TAG.toString(),MarpleRelationships.EDGE_FILE_HAS_TAG);
	      sEdgeMap.put(EdgeType.EDGE_MEMORY_AFFECTS_EVENT.toString(),MarpleRelationships.EDGE_MEMORY_AFFECTS_EVENT);
	      sEdgeMap.put(EdgeType.EDGE_NETFLOW_HAS_TAG.toString(),MarpleRelationships.EDGE_NETFLOW_HAS_TAG);
	      sEdgeMap.put(EdgeType.EDGE_OBJECT_PREV_VERSION.toString(),MarpleRelationships.EDGE_OBJECT_PREV_VERSION);
	      sEdgeMap.put(EdgeType.EDGE_SRCSINK_AFFECTS_EVENT.toString(),MarpleRelationships.EDGE_SRCSINK_AFFECTS_EVENT);
	      sEdgeMap.put(EdgeType.EDGE_SRCSINK_HAS_TAG.toString(),MarpleRelationships.EDGE_SRCSINK_HAS_TAG);
	      sEdgeMap.put(EdgeType.EDGE_SUBJECT_AFFECTS_EVENT.toString(),MarpleRelationships.EDGE_SUBJECT_AFFECTS_EVENT);
	      sEdgeMap.put(EdgeType.EDGE_SUBJECT_HAS_TAG.toString(),MarpleRelationships.EDGE_SUBJECT_HAS_TAG);
	      sEdgeMap.put(EdgeType.EDGE_SUBJECT_HASLOCALPRINCIPAL.toString(),MarpleRelationships.EDGE_SUBJECT_HASLOCALPRINCIPAL);
	      sEdgeMap.put(EdgeType.EDGE_SUBJECT_HASPARENT_SUBJECT.toString(),MarpleRelationships.EDGE_SUBJECT_HASPARENT_SUBJECT);
	      sEdgeMap.put(EdgeType.EDGE_SUBJECT_RUNSON.toString(),MarpleRelationships.EDGE_SUBJECT_RUNSON);
	      sEdgeMap.put(EdgeType.EDGE_NETFLOW_AFFECTS_EVENT.toString(),MarpleRelationships.EDGE_NETFLOW_AFFECTS_EVENT);
	      sEdgeMap.put(EdgeType.EDGE_MEMORY_HAS_TAG.toString(), MarpleRelationships.EDGE_MEMORY_HAS_TAG);
	      
	      
	  }
	  
		public void setTopic(String topic) {
			this.topic = topic;
			
		}

	  
	  /**
	   * 
	   * @param datum - The CDM JSON deserialized as a map.
	   * @param props - The Neo4J properties for the current vertex or relationship.
	   */
	  private void flattenScores
	  (
		 Map<String, String> datum
		,HashMap<String,Object> props)
	  {
		if (datum.containsKey("scores")) {
			Object scores = datum.get("scores");
			ArrayList<Map<String, Object>> scoreArray = (ArrayList<Map<String, Object>>) scores;
			for (int i = 0; i < scoreArray.size(); i++) {
				Map<String, Object> score = scoreArray.get(i);
				for (String key : score.keySet()) {
					Object val = score.get(key);
					if ((val instanceof String) || (val instanceof Number)) {
						props.put(key + "." + i, score.get(key));
					} else if (val instanceof Map) {
						Map<String, Object> evidence = (Map<String, Object>) score.get(key);
						for (String subKey : evidence.keySet()) {
							props.put(key + "." + i +"."+ subKey, evidence.get(subKey));
						}
					}
				}
			}
		}
	  }
		
	  
	  public boolean isFileObject(Map<String,String> datum)
	  {
		  return  (datum.containsKey("version"));		  
	  }
	  
	  public boolean isProvProvenanceTag(Map<String,String> datum)
	  {
		  return (datum.containsKey("tagId"));
	  }
	  
	  public boolean isNetFlowObject(Map<String,String> datum)
	  {
		  boolean isNetFlow=false;
		  return (datum.containsKey("srcAddress") &&
			  datum.containsKey("dstAddress") &&
			  datum.containsKey("srcPort") &&
			  datum.containsKey("dstPort"));
	  }
	  
	  public boolean isMemoryObject(Map<String,String> datum)
	  {
		  return (datum.containsKey("pageNum") &&
				  datum.containsKey("address"));
				  
	  }
	  
	  public void storeEx(Map<String,Object> record)
	  {
		  HashMap<String,Object> props = new HashMap<String,Object>();
		  Map<String,String> datum = (Map<String, String>) record.get("datum");
		  //System.out.println("dataum is an instanceof: "+dat.getClass().getName());
		  //Map<String,String> datum = (Map<String,String>)record.getDatum();
		  String recType = datum.get("type");
		  if (recType==null)
		  {
			  recType = datum.get("Type");
		  }
		  if (recType==null)
		  {
			  /* 
			   * Host, FileObject, NetFlowObject,Object,Memory, ProvananceTag and Value 
			   * do not have a "type" property.  Consequently, when deserialized as JSON 
			   * they become indistinguishable from one another.
			   * 
			   * A ProvProvenanceTag will have a tagId field.  This is uniq.
			   * A FileObject has a version field.
			   * A NetFlowObject has srcAddress,srcPort, dstAddress, and dstPort fields.
			   * A MemoryObject has pageNum and address fields.
			   * 
			   */
			  if (isProvProvenanceTag(datum))
			  {
				  processProvenanceTag(datum);
			  } else if (isFileObject(datum))
			  {
				  processFileObject(datum);
			  } else if (isNetFlowObject(datum))
			  {
				  processNetFlowObject(datum);
			  } else if (isMemoryObject(datum))
			  {
				  processMemoryObject(datum);
			  } else {
				  log.equals("Could not decode record.  Unknown type:"+datum);
				  for (String key: datum.keySet())
				  {
					  log.debug("{"+key+":"+datum.get(key)+"}");
				  }
			  }
			  
		  } else if (recType.startsWith("PRINCIPAL_")) {
			  processPrincipal(datum,props);
		  } else if (recType.startsWith("SUBJECT_")) {
			  processSubject(datum,recType);
		  } else if (recType.startsWith("EDGE")) {
			  processEdge(datum,recType);
		  } else if (recType.startsWith("EVENT_")) {
			  processEvent(datum,recType);
		  } else if (recType.startsWith("SOURCE_")) {
			  processSrcSink(datum);
		  } else if (recType.equalsIgnoreCase("FileObject")) {
			  processFileObject(datum);
		  } else if (recType.equalsIgnoreCase("ProvenanceTagNode")) {
			  processProvenanceTag(datum);
		  } else if (recType.equalsIgnoreCase("NetFlowObject")) {
			  processNetFlowObject(datum);
		  } else if (recType.equalsIgnoreCase("MemoryObject")) {
			  processMemoryObject(datum);
		  } else if (recType.equalsIgnoreCase("RegistryKeyObject")) {
			  processRegistryKeyObject(datum);
		  } else {
			  log.equals("Could not decode record.  Unknown type:"+datum);
			  for (String key: datum.keySet())
			  {
				  log.debug("{"+key+":"+datum.get(key)+"}");
			  }
		  }
		  
	  }
	  
	  
	private void processRegistryKeyObject(Map<String, String> datum) {
		Object uuid =  datum.get("uuid");
		CUUID cuuid = new CUUID((String)uuid,MarpleLabels.RegistryKeyObject.toString());
		HashMap<String,Object> props = new HashMap<String,Object>();
		 Long vertex = vertexMap.get(cuuid);
		 if (vertex==null)
		 {
			  props.put("uuid",cuuid.getHexUUID());
			  props.put("version",datum.get("version")==null ? "null" : datum.get("version"));
			  vertex = Long.valueOf(bi.createNode(props,MarpleLabels.RegistryKeyObject));
			  vertexMap.put(cuuid,vertex);	
		 }
		
		
	}



	private void processMemoryObject(Map<String, String> datum) {
		Object uuid =  datum.get("uuid");
		CUUID cuuid = new CUUID((String)uuid,MarpleLabels.MemoryObject.toString());
		HashMap<String,Object> props = new HashMap<String,Object>();
		 Long vertex = vertexMap.get(cuuid);
		 if (vertex==null)
		 {
			  props.put("uuid",cuuid.getHexUUID());
			  //props.put("pageNum",item.getPageNumber()!=null ? item.getPageNumber() : -1L);
			  //props.put("address", item.getMemoryAddress());
			  //props.put("baseObject.source", item.getBaseObject().getSource().name());
				Object baseObject = datum.get("baseObject");
				logObjectType("baseObject", baseObject);
				/*
			  SHORT p = item.getBaseObject().getPermission();
			  if (Arrays.asList(p) != null)
			  {
				  saveShorts(Arrays.asList(p),props,"baseObject.permission");
			  } else  {
				  props.put("baseObject.permission",item.getBaseObject().getPermission() !=null ? item.getBaseObject().getPermission()  : "null");
			  }
			  props.put("baseObject.lastTimestampMicros", item.getBaseObject().getLastTimestampMicros() !=null ? item.getBaseObject().getLastTimestampMicros() : "null");
			  storeVertexItemProps(item.getBaseObject().getProperties(),props);
			  */
			  vertex = Long.valueOf(bi.createNode(props,MarpleLabels.MemoryObject));
			  vertexMap.put(cuuid,vertex);	

		 } else {
			  RuntimeException re = new RuntimeException("WARNING: duplicate MemoryOject UUID: "+cuuid.getUUID().toString());
			  re.printStackTrace();				  			  				 
		 }

		
	}

	private void logObjectType(String key,Object item)
	{
		if (item!=null)
		{ 
			log.info(key +" is an instance of "+item.getClass().getName());
		} else {
			log.info(key +" is null.");
		}
	}
	  
	/**
	 * 1. Construct a uniq UUID/CUUID for the ProvenanceTagNode (it is not implict).
	 * 2. Store [ UUID | TagOpCode | Integrity | Confidentiality ], tagId (int), & children.
	 * @param datum
	 */
	private CUUID processProvenanceTag(Map<String, String> datum) 
	{
		HashMap<String,Object> props = new HashMap<String,Object>();
		Object tagIdObj = datum.get("tagId");
		Double tagId = (Double)tagIdObj;
		UUID tmp = new UUID(String.valueOf(tagId).getBytes());
		CUUID cuuid = new CUUID(tmp);
		CUUID ptnCuuid=null;
		UUID ptnUuid=null;
		ptnUuid = getUUID();
		ptnCuuid = new CUUID(ptnUuid,MarpleLabels.ProvenanceTagNode.toString());
		Long vertex = vertexMap.get(ptnCuuid);
		if (vertex==null)
		{
			  props.put("uuid",cuuid.getHexUUID());
			  props.put("tagId", datum.get("tagId"));
			  String key="value";
			  Object itemValue =null;
			  if (datum.containsKey("value"))
			  {
				 itemValue= datum.get("value");
			  } else if (datum.containsKey("uuid"))
			  {
				  itemValue = datum.get("uuid");
				  key = "uuid";
			  } else if (datum.containsKey("IntegrityTag"))
			  {
				  itemValue = datum.get("IntegrityTag");
				  key = "IntegrityTag";
			  } else if (datum.containsKey("ConfidentialityTag"))
			  {
				  itemValue = datum.get("ConfidentialityTag");
				  key ="ConfidentialityTag";
			  }
			  if (itemValue instanceof String) {
				  props.put(key,(String)itemValue);
			  } else if (itemValue instanceof UUID)
			  {
				  props.put(key, new CUUID((UUID)itemValue).getHexUUID());
			  } else if (itemValue instanceof int[])
			  {
				  props.put(key, Arrays.toString((int[])itemValue));
			  } else if (itemValue instanceof long[])
			  {
				  props.put(key, Arrays.toString((long[])itemValue));
			  }
			  // TODOD:  storeVertexItemProps(item.getProperties(),props);
			  vertex = Long.valueOf(bi.createNode(props,MarpleLabels.ProvenanceTagNode));
			  vertexMap.put( ptnCuuid ,vertex);
			  if (datum.containsKey("children"))
			  {
				  Object cObject = datum.get("children");
				  if (cObject.getClass().isArray())
				  {
					  ArrayList<Map<String, String>> children = (ArrayList<Map<String, String>>)cObject;
					  // TODO: FINIS THIS IMPL.
					  if (children!=null)
					  {
						  for (int i=0;i<children.size();i++)
						  {
							  Map<String,String> childDatum = (Map<String,String>) children.get(i);
							  CUUID cNode = processProvenanceTag(childDatum);
							  Long toVertex = vertexMap.get(cNode);
							  Long fromVertex = vertex;
							  long relId =	bi.createRelationship( fromVertex,toVertex,MarpleRelationships.EDGE_EVENT_HAS_TAG ,new HashMap<String,Object>());	
						  }
					  }
				  }
			  }

		}
		return ptnCuuid;
	}

	private void processFileObject(Map<String, String> datum) 
	{
		Object uuid =  datum.get("uuid");
		CUUID cuuid = new CUUID((String)uuid,MarpleLabels.FileObject.toString());
		HashMap<String,Object> props = new HashMap<String,Object>();
		Long vertex = vertexMap.get(cuuid);
		if (vertex==null)
		{
			props.put("version", datum.get("version")==null? "null" : datum.get("version"));
			props.put("url", datum.get("url")==null? "null": datum.get("url"));
			props.put("size",datum.get("size")!=null ? datum.get("size") : -1 );
			props.put("uuid", cuuid.getHexUUID());
			props.put("baseObject.source", datum.get("baseObject.source")!=null ? datum.get("baseObject.source") : "null");
			props.put("baseObject.permission", datum.get("baseObject.permission") !=null ? datum.get("baseObject.permission") : "null");
			props.put("baseObject.lastTimestampMicros", datum.get("baseObject.lastTimestampMicros") !=null ? datum.get("baseObject.lastTimestampMicros") : "null");

			flattenScores(datum,props);
			vertex = Long.valueOf(bi.createNode(props,MarpleLabels.FileObject));
			vertexMap.put(cuuid, vertex);
		}
	}

	private void processNetFlowObject(Map<String, String> datum) 
	{
 		Object uuid =  datum.get("uuid");
		CUUID cuuid = null; 
        if (uuid instanceof ArrayList)
		{
		  ArrayList<Double> uuidList = (ArrayList)uuid;
		  cuuid = uuidFromArrayList(uuidList);
		} else {
			
		  cuuid = new CUUID((String)uuid,MarpleLabels.NetflowObject.name());
		}
		HashMap<String,Object> props = new HashMap<String,Object>();
		
		Long vertex = vertexMap.get(cuuid);
		if (vertex==null)
		{
			props.put("uuid",cuuid.getHexUUID());
			props.put("baseObject.source", datum.get("baseObject.source")!=null ? datum.get("baseObject.source") : "null");
			props.put("baseObject.srcAddress", datum.get("baseObject.srcAddress")!=null ? datum.get("baseObject.srcAddress") : "null");
			props.put("baseObject.srcPort", datum.get("baseObject.srcPort")!=null?datum.get("baseObject.srcPort") : "null");
			props.put("baseObject.destAddress", datum.get("baseObject.destAddress") !=null ? datum.get("baseObject.destAddress") : "null");
			props.put("baseObject.destPort", datum.get("baseObject.destPort") !=null ? datum.get("baseObject.destPort") : "null");
			props.put("baseObject.permission", datum.get("baseObject.permission") !=null ? datum.get("baseObject.permission") : "null");
			props.put("baseObject.lastTimestampMicros", datum.get("baseObject.lastTimestampMicros") !=null ? datum.get("baseObject.lastTimestampMicros") : "null");
//			Object baseObject = datum.get("baseObject");
//			logObjectType("baseObject", baseObject);
			flattenScores(datum,props);
			vertex = Long.valueOf(bi.createNode(props,MarpleLabels.NetflowObject));
			vertexMap.put(cuuid,vertex);

		}
	}

	private void processSrcSink(Map<String, String> datum) 
	{
		Object uuid =  datum.get("uuid");
		ArrayList<Double> uuidList = (ArrayList)uuid;
		CUUID cuuid = uuidFromArrayList(uuidList);
		HashMap<String,Object> props = new HashMap<String,Object>();
		
		Long vertex = vertexMap.get(cuuid);
		if (vertex==null)
		{
			props.put("type",datum.get("type"));
			props.put("uuid", cuuid.getHexUUID());
			flattenScores(datum,props);

			vertex = Long.valueOf(bi.createNode(props,MarpleLabels.SrcSinkObject));
			vertexMap.put(cuuid,vertex);
		}
		
	}
	
	

	private void processEvent(Map<String, String> datum,String eventType) 
	{
		Object uuid =  datum.get("uuid");
		CUUID cuuid = new CUUID((String)uuid,sEventMap.get(eventType).toString());
		HashMap<String,Object> props = new HashMap<String,Object>();
	      props.put("name",datum.get("name")!=null ? datum.get("name"): "null");
		  props.put("type",eventType);
		  props.put("location",datum.get("location") != null ? datum.get("location") : -1);
		  props.put("sequence", datum.get("sequence"));
		  props.put("threadId",datum.get("threadId"));
		  props.put("source", datum.get("source"));
		  props.put("timestampMicros",datum.get("timestampMicros"));
		  props.put("size",datum.get("size") !=null ? datum.get("size") : -1 );
		  flattenScores(datum,props);

		  Long vertex = Long.valueOf(bi.createNode(props,sEventMap.get(eventType)));
		  vertexMap.put(cuuid,vertex);
		/* TODO implement these...
				  saveParameters(item.getParameters(),props,"parameters");
				  props.put("programPoint",item.getProgramPoint() !=null ?"\""+ item.getProgramPoint()+"\"" : "null");
				  
				  CharSequence ppt = item.getProgramPoint();
				  String val = "{"+ppt.toString()+"}";
				  try {
					  Properties p = gson.fromJson(val, Properties.class);
					  Iterator<Object> keyIter = p.keySet().iterator();
					  while (keyIter.hasNext())
					  {
						   String key= (String) keyIter.next();
						   String nVal = p.getProperty(key);
						   String nKey = "pgmPt."+key;
						   props.put(nKey,nVal);
					  }
				  } catch (Exception e) {
					  props.put("programPoint","\""+item.getProgramPoint()+"\"" !=null ? item.getProgramPoint() : "null");
				  }				  
				  storeVertexItemProps(item.getProperties(),props);

		 */
	}

	private void processEdge(Map<String, String> datum,String edgeType) 
	{
		Object fromUuidObject =  datum.get("fromUuid");
		CUUID fromCuuid = new CUUID((String)fromUuidObject,"");
		Object toUuidObject = datum.get("toUuid");
		CUUID toCuuid = new CUUID((String)toUuidObject,"");
		
		Long fromVertex = vertexMap.get(fromCuuid);
		Long toVertex = vertexMap.get(toCuuid);
		HashMap<String,Object> props = new HashMap<String,Object>();
		props.put("timestamp", datum.get("timestamp"));
		if (fromVertex == null )
		{
			  //throw new RuntimeException("item.getFromUuid() not found in vertexMap!");
			  System.out.println("From vertex not found in vertexMap!  Dropping edge.  fromUUID: "+fromCuuid.getHexUUID() );
			  return;
		}
		if (toVertex == null )
		{
			  System.out.println("To vertex not found in vertexMap!  Dropping edge.   toUUID: "+ toCuuid.getHexUUID());
			  return;
		}
		flattenScores(datum,props);
		long relId = bi.createRelationship( fromVertex,toVertex,sEdgeMap.get(edgeType) ,props);			  

		
	}
	
	private CUUID uuidFromArrayList(ArrayList<Double> uuidList)
	{
		CUUID cuuid = null;
		byte uuidBytes[] = new byte[uuidList.size()];
		for (int i=0;i<uuidBytes.length;i++)
		{
			uuidBytes[i] = uuidList.get(i).byteValue();
		}
		UUID uu = new UUID(uuidBytes);
		cuuid = new CUUID(uu);
		return cuuid;
	}

	private void processSubject(Map<String, String> datum,String type) 
	{
		Object uuid =  datum.get("uuid");
		if (uuid instanceof String)
		{
			CUUID cuuid = new CUUID((String)uuid,type);
		    // OY!  now I have a UUID.
			Long vertex = vertexMap.get(cuuid);
			if (vertex==null)
			{
				HashMap<String,Object> props = new HashMap<String,Object>();
				props.put("uuid",cuuid.getHexUUID());
				props.put("type", type);
				props.put("source", datum.get("source")!=null ? datum.get("source") : "");
				props.put("startTimestampMicros", datum.get("startTimestampMicros") !=null ? datum.get("startTimestampMicros") : -1);
				props.put("endTimestampMicros", datum.get("endTimestampMicros") !=null ? datum.get("endTimestampMicros") : -1);
				props.put("unitId", datum.get("unitId") != null ? datum.get("unitId") : "null" );
				props.put("cmdLine",datum.get("cmdLine")!= null ? "\""+datum.get("cmdLine")+"\"" : "null" );
				props.put("importedLibraries", datum.get("importedLibraries")!= null ?  datum.get("importedLibraries") : "null");
				props.put("exportedLibraries", datum.get("exportedLibraries")!= null ? datum.get("exportedLibraries") : "null");
				props.put("pInfo", datum.get("pInfo")!= null ? datum.get("pInfo") : "null" );
				Label neoType = sSubjectMap.get(type);
				flattenScores(datum,props);
				vertex = Long.valueOf(bi.createNode(props,neoType));
				vertexMap.put(cuuid,vertex);
				// TODO: fixup the vertex properties here.
				//storeVertexItemProps((Map<CharSequence,CharSequence>)datum.("properties"),props);
								
			}
		} else {
			log.error("Unexpected UUID datatype: "+uuid.getClass().getName());
		}
		
	}

	private void processPrincipal(Map<String, String> datum, HashMap<String,Object> props) 
	{
		Object uuid =  datum.get("uuid");
		if (uuid instanceof ArrayList)
		{
			ArrayList<Double> uuidList = (ArrayList)uuid;
			CUUID cuuid = uuidFromArrayList(uuidList);
		    // OY!  now I have a UUID.
			Long vertex = vertexMap.get(cuuid);
			if (vertex==null)
			{
				props.put("uuid",cuuid.getHexUUID());
				props.put("type", datum.get("type"));
				props.put("userId",datum.get("userId"));
			    Object groupIds = datum.get("groupIds");
			    logObjectType("groupIds", groupIds);
			    props.put("source", datum.get("source"));
				flattenScores(datum,props);

				//props.put("groupIds", value)
			    // TODO: storeVertexItemProps(item.getProperties(),props);
			    vertex = Long.valueOf(bi.createNode(props,MarpleLabels.Principal));
			    vertexMap.put(cuuid,vertex);
			}
		} else if (uuid instanceof String)
		{
			CUUID cuuid = new CUUID((String)uuid,MarpleLabels.Principal.toString());
			  Long vertex = vertexMap.get(cuuid);
			  if (vertex==null)
			  {
				props.put("uuid",cuuid.getHexUUID());
				props.put("type", datum.get("type"));
				props.put("userId",datum.get("userId"));
			    Object groupIds = datum.get("groupIds");
			    logObjectType("groupIds", groupIds);
			    props.put("source", datum.get("source"));
			    // FIXME!
				//storeVertexItemProps(datum.get("Properties"),props);
				flattenScores(datum,props);

			    vertex = Long.valueOf(bi.createNode(props,MarpleLabels.Principal));
				vertexMap.put(cuuid,vertex);
				rVertexMap.put(vertex, cuuid);
				if (vertex<0)
				{
					System.err.println("negative node id returned for create node.");
				}
			  } else {
				  System.err.println("WARNING: existing vertex for UUID encountered. Potential property loss.");
			  }
		}
		
	}

	private void exploreSchema(Schema schema)
	  {
		  //schema.
	  }
	  
	  public Neo4JProcessor(Properties props) throws IOException 
	  {
		  g_props = props;
	      debug = Boolean.valueOf(g_props.getProperty("debug", "False"));

		   boolean aborted=false;
		   bi = BatchInserters.inserter(new File(g_props.getProperty("DEFAULT_STORE_FOLDER")));
		   long start = Calendar.getInstance().getTimeInMillis();
		   long end = Calendar.getInstance().getTimeInMillis(); 
		   beginning=start;
		   
		   nodeIds.put("Vertices",vertexMap);  
		   end = Calendar.getInstance().getTimeInMillis();
		   String msg = "Created/Opened database elapsed time: "+((end-start) / 1000)+"s\n";
		   log.info(msg);
	  }
	  
	  public void open() throws IOException
	  {
		   boolean aborted=false;
		   long start = Calendar.getInstance().getTimeInMillis();
		   long end = Calendar.getInstance().getTimeInMillis(); 

		   bi = BatchInserters.inserter(new File(g_props.getProperty("DEFAULT_STORE_FOLDER")));
		   end = Calendar.getInstance().getTimeInMillis(); 
		   String msg = "Created/Opened database elapsed time: "+((end-start) / 1000)+"s\n";
		 		  
	  }
	  
	  private void storeVertexItemProps(Map<CharSequence,CharSequence> itemProps,HashMap<String,Object> props)
	  {
		    if (itemProps==null)
		    	return;
			Iterator<CharSequence> keyIter = itemProps.keySet().iterator();
			while (keyIter.hasNext())
			{
				  CharSequence csKey = keyIter.next();
				  String key = csKey.toString();
				  props.put(key,itemProps.get(csKey).toString());
			}
			return;
	  }
	  
	  
	  public int[] convertIntegerList(List<Integer> list)
	  {
		  int lar[] = new int[list.size()];
		  int index=0;
		  Iterator<Integer> listIter = list.iterator(); 
	      while (listIter.hasNext())
		  {
				lar[index]=listIter.next();
			    index+=1;
		  }
	      return lar;

	  }
	  
	  public String[] convertCharSequenceList(List<CharSequence> list)
	  {
		  String lar[] = new String[list.size()];
		  int index=0;
		  Iterator<CharSequence> listIter = list.iterator(); 
	      while (listIter.hasNext())
		  {
				lar[index]=listIter.next().toString();
			    index+=1;
		  }
	      return lar;	  
	  }

	  int ctr=0;
	  

	  
	  private void saveParameters(String syscallName, List<Value> parms, HashMap<String,Object> props, String key)
		  {
	//		  if (parms == null)
	//			  return;
	//		  StringBuffer sb = new StringBuffer();
	//		  int index=0;
	//		  for (Value val : parms)
	//		  {
	//			  if (index>0)
	//				  sb.append(",");
	//			  index+=1;
	//			  sb.append(val.toString());
	//		  }
	//		  props.put(key, sb.toString());
			  
			  StringBuffer sb = new StringBuffer();
			  int index=0;
			  if (parms != null) {
					for (int j = 0; j < parms.size(); j++) {
						if (index>0)
							  sb.append(",");
						  index+=1;
						  
						ValueDataType dataType = parms.get(j).getValueDataType();
						ByteBuffer data = (ByteBuffer)parms.get(j).getValueBytes();
						switch (dataType) {
						case VALUE_DATA_TYPE_COMPLEX:
							String stringParm = parms.get(j).getName().toString();
							sb.append(stringParm);
							break;
						
						default:
							byte[] valueBytes_bytes = new byte[data.remaining()];
							data.get(valueBytes_bytes);
							String dataString = DatatypeConverter.printHexBinary(valueBytes_bytes);
							sb.append(dataString);
							break;
						}
					}
			  }
			  props.put(key, sb.toString());
		  }


	private void createEdgesByParms(Long eventVertex, String syscallName, List<Value> parms){
		if (parms != null) {
			for (int j = 0; j < parms.size(); j++) {				  
				ValueDataType dataType = parms.get(j).getValueDataType();
				ByteBuffer data = (ByteBuffer)parms.get(j).getValueBytes();
				switch (dataType) {
				case VALUE_DATA_TYPE_COMPLEX:
					String stringParm = parms.get(j).getName().toString();
					createEdgesByParm(eventVertex, syscallName, stringParm);
					break;
				
				default:
					byte[] valueBytes_bytes = new byte[data.remaining()];
					data.get(valueBytes_bytes);
					String dataString = DatatypeConverter.printHexBinary(valueBytes_bytes);
					createEdgesByParm(eventVertex, syscallName, dataString);
					break;
				}
			}
	  }
	}
	  
	private void createEdgesByParm(Long eventVertex, String syscallName, String parm){
		  HashMap<String,Object> vertexProps = new HashMap<String, Object>();
		  Long fromVertex = eventVertex;
		  Long toVertex = -1L;
		  
		  if (parm.contains("ipAddress") && parm.contains("port"))
		  {
			  String ipAddress = parm.substring(parm.indexOf("ipAddress")+10, parm.indexOf("@BufferCount"));
			  String port = parm.substring(parm.indexOf("port")+5, parm.indexOf("@ipAddress"));
			  IpPortObject netflow = new IpPortObject(ipAddress, port);
			  if (!this.netflowVertexMap.containsKey(netflow))
			  {
				  vertexProps.put("ip", ipAddress);
				  vertexProps.put("port", port);
				  Long vertex = Long.valueOf(bi.createNode(vertexProps, MarpleLabels.NetflowObject));
				  this.netflowVertexMap.put(netflow, vertex);
				
				  toVertex = vertex;				 
			  }
			  else 
			  {
				  toVertex = this.netflowVertexMap.get(netflow);
			  }
			  
			  // event -> netflowobject		  
			  bi.createRelationship( fromVertex,toVertex,MarpleRelationships.EDGE_EVENT_AFFECTS_NETFLOW , new HashMap<String, Object>());
			  
			  if (!this.eventVertex2Parms.containsKey(eventVertex))
			  {
				  this.eventVertex2Parms.put(eventVertex, new ArrayList<Long>());				  
			  }
			  this.eventVertex2Parms.get(eventVertex).add(toVertex);
			  
			  
		  }
		  else if (parm.contains("\\"))
		  {
			  if (!this.unknowObjectVertexMap.containsKey(parm))
			  {
				  vertexProps.put("path", parm);
				  Long vertex = Long.valueOf(bi.createNode(vertexProps, MarpleLabels.UnknowObject));
				  this.unknowObjectVertexMap.put(parm, vertex);
				  
				  toVertex = vertex;
			  }
			  else 
			  {
				  toVertex = this.unknowObjectVertexMap.get(parm);
			  }			 
			  
			  // event -> unknowobject			
			  bi.createRelationship( fromVertex,toVertex,MarpleRelationships.EDGE_EVENT_AFFECTS_OBJECT , new HashMap<String, Object>());
			  
			  if (!this.eventVertex2Parms.containsKey(eventVertex))
			  {
				  this.eventVertex2Parms.put(eventVertex, new ArrayList<Long>());				  
			  }
			  this.eventVertex2Parms.get(eventVertex).add(toVertex);
		  }
		  
	  }
	  
	  private void saveShorts(List<SHORT> shorts,HashMap<String,Object> props, String key)
	  {
		  if (shorts == null)
			  return;
		  StringBuffer sb = new StringBuffer();
		  int index=0;
		  for (SHORT val : shorts)
		  {
			  if (index>0 && val!=null)
			  {
				  sb.append(",");
			  index+=1;
			  }
			  if (val!=null)
			  {
			     sb.append(String.valueOf(val.toString()));
			  }
		  }
		  props.put(key, sb.toString());
		  
	  }
	  
	  /**
	   * 
	   * Convert the string back to a TCCDMDatum object.
	   * @param json - String of deserialized JSON from AVRO
	   * 
	   */
	  public void store(String json)
	  {
		  Gson gson = new GsonBuilder().enableComplexMapKeySerialization().create();
		  LinkedHashMap<String,Object> map = gson.fromJson(json, LinkedHashMap.class);
		  storeEx(map);
		  //Object datum = (Object) map.get("datum");
		  //String recType = datum.get("type");

		  //TCCDMDatum record = gson.fromJson(json, TCCDMDatum.class);
		  //store(record);
	  }
	  
	  /**
	   * Extract the CDM properties for the principal object and create Labeled node
	   * of type "Principal".
	   * 
	   * @param item - The Principal to be stored as vertex in the graph
	   * @param props - The properties object to store the principal attributes into.
	   */
	  private void processPrincipal(Principal item, HashMap<String,Object> props)
	  {
		  CUUID cuuid = new CUUID(item.getUuid(),MarpleLabels.Principal.toString());
		  Long vertex = vertexMap.get(cuuid);
		  if (vertex==null)
		  {
			props.put("uuid",cuuid.getHexUUID());
			props.put("userId",item.getUserId().toString());			
			Iterator<CharSequence> gidIter = item.getGroupIds().iterator();
			if (gidIter.hasNext()) {
			    String gids[] = convertCharSequenceList(item.getGroupIds());
				props.put("groupIds",gids);
			}
			props.put("source", item.getSource().name());
			storeVertexItemProps(item.getProperties(),props);
		    vertex = Long.valueOf(bi.createNode(props,MarpleLabels.Principal));
			vertexMap.put(cuuid,vertex);
			rVertexMap.put(vertex, cuuid);
			if (vertex<0)
			{
				System.err.println("negative node id returned for create node.");
			}
		  } else {
			  System.err.println("WARNING: existing vertex for UUID encountered. Potential property loss.");
		  }
	  }
	  
	  /**
	   * Extract the CDM properties for the Subject object and create Labeled node
	   * of type "SUBJECT_PROCESS".
	   * 
	   * @param item - The Principal to be stored as vertex in the graph
	   * @param props - The properties object to store the principal attributes into.
	   */
	  private void processSubject(Subject item,HashMap<String,Object> props)
	  {
		  CUUID cuuid = new CUUID(item.getUuid(),item.getType().name());
		  Long vertex = vertexMap.get(cuuid);
		  if (vertex==null)
		  {
			  props.put("uuid",cuuid.getHexUUID());
			  props.put("Type",item.getType().name());
			  props.put("pid",item.getPid() );
			  props.put("ppid", item.getPpid());
			  props.put("source", item.getSource().name());
			  props.put("startTimestampMicros",item.getStartTimestampMicros() !=null ? item.getStartTimestampMicros() : -1);
			  props.put("endTimestampMicros", item.getEndTimestampMicros() !=null ? item.getEndTimestampMicros() : -1);
			  props.put("unitId", item.getUnitId() != null ? item.getUnitId() : "null" );
			  props.put("cmdLine",item.getCmdLine() != null ? "\""+item.getCmdLine()+"\"" : "null" );
			  props.put("importedLibraries", item.getImportedLibraries()!= null ?  item.getImportedLibraries() : "null");
			  props.put("exportedLibraries", item.getExportedLibraries()!= null ? item.getImportedLibraries() : "null");
			  props.put("pInfo", item.getPInfo() != null ? "\""+item.getPInfo()+"\"" : "null" );
			  storeVertexItemProps(item.getProperties(),props);
			  vertex = Long.valueOf(bi.createNode(props,subjectMap.get(item.getType())));
			  vertexMap.put(cuuid,vertex);
			  rVertexMap.put(vertex, cuuid);
			  
			  // create relationship between processes
			  int subjectPid = item.getPid();
			  int subjectPpid = item.getPpid();
			  if (!this.pid2pidVertex.containsKey(subjectPid))
			  {
				  this.pid2pidVertex.put(subjectPid, new ArrayList<Long>());
			  }
			  this.pid2pidVertex.get(subjectPid).add(vertex);
			  
			  if (!this.ppid2pid.containsKey(subjectPpid))
			  {
				  this.ppid2pid.put(subjectPpid, new ArrayList<Integer>());
			  }
			  System.out.println("=============" + this.ppid2pid.get(subjectPpid));
			  this.ppid2pid.get(subjectPpid).add(subjectPid);
			  
			  for (int pid : this.pid2pidVertex.keySet())
			  {
				  if (pid == subjectPpid)
				  {
					  for (Long toVertex : this.pid2pidVertex.get(pid))
					  {
						  Long fromVertex = vertex;
						  bi.createRelationship(fromVertex, toVertex, MarpleRelationships.EDGE_SUBJECT_HASPARENT_SUBJECT, new HashMap<String,Object>());

					  }
				  }
			  }
			  
			  for (int ppid : this.ppid2pid.keySet())
			  {
				  if (ppid == subjectPid)
				  {
					  for (Integer pid : this.ppid2pid.get(ppid))
					  {
						  for (Long fromVertex : this.pid2pidVertex.get(pid))
						  {
							  Long toVertex = vertex;
							  bi.createRelationship(fromVertex, toVertex, MarpleRelationships.EDGE_SUBJECT_HASPARENT_SUBJECT, new HashMap<String,Object>());
						  }
					  }
				  }
			  }
		  } else {
			  RuntimeException re = new RuntimeException("WARNING: duplicate subject UUID: "+cuuid.getUUID().toString());
			  re.printStackTrace();				  
		  }
	  }
	  
	  /**
	   * Extract the CDM properties for the Event object and create Labeled node
	   * of type "EVENT_XXX".   @see {@link #eventMap}
	   * 
	   * @param item - The Event to be stored as vertex in the graph
	   * @param props - The properties object to store the Event attributes into.
	   */
	  private void processEvent(Event item, HashMap<String,Object> props)
	  {
		  Label l = eventMap.get(item.getType());
		  if (l==null)
		  {
			  System.err.println("ERROR: unknown event type: "+item.getType()+" Event dropped!");
			  return;
		  }

		  CUUID cuuid = new CUUID(item.getUuid(),l.toString());

		  Long vertex = vertexMap.get(cuuid);
		  if (vertex==null)
		  {
			  props.put("uuid",cuuid.getHexUUID());
			  props.put("name",item.getName()!=null ? "\""+ item.getName()+"\"" : "null");
			  props.put("Type",item.getType().name());
			  props.put("location",item.getLocation() != null ? item.getLocation() : -1);
			  saveParameters(item.getName().toString(), item.getParameters(),props,"parameters");
			  props.put("programPoint",item.getProgramPoint() !=null ?"\""+ item.getProgramPoint()+"\"" : "null");
			  props.put("sequence", item.getSequence());
			  props.put("threadId",item.getThreadId());
			  props.put("source", item.getSource().name());
			  props.put("timestampMicros",item.getTimestampMicros() == null ? Long.valueOf(-1L) : item.getTimestampMicros() );
			  props.put("size",item.getSize() !=null ? item.getSize() : -1 );
			  storeVertexItemProps(item.getProperties(),props);
			  vertex = Long.valueOf(bi.createNode(props,l));
			  vertexMap.put(cuuid,vertex);
			  rVertexMap.put(vertex, cuuid);
			  
			  // create edges by parms
			  createEdgesByParms(vertex, item.getName().toString(), item.getParameters());
		  } else {
			  RuntimeException re = new RuntimeException("WARNING: duplicate Event UUID: "+cuuid.getUUID().toString());
			  re.printStackTrace();				  			  
		  }
	  }
	  
	  /**
	   * Extract the CDM properties for the NetFlowObject object and create Labeled node
	   * of type "NetFlowObject".   
	   * 
	   * @param item - The NetFlowObject to be stored as vertex in the graph
	   * @param props - The properties object to store the NetFlowObject attributes into.
	   */
	  private void processNetFlowObject(NetFlowObject item, HashMap<String,Object> props)
	  {
		  CUUID cuuid = new CUUID(item.getUuid(),MarpleLabels.NetflowObject.toString());
		  Long vertex = vertexMap.get(cuuid);
		  if (vertex==null)
		  {
			  props.put("baseObject.source", item.getBaseObject().getSource().name());
			  SHORT p = item.getBaseObject().getPermission();
			  if (Arrays.asList(p) != null)
			  {
				  saveShorts(Arrays.asList(p),props,"baseObject.permission");
			  } else  {
				  props.put("baseObject.permission",item.getBaseObject().getPermission() !=null ? item.getBaseObject().getPermission()  : "null");
			  }
			  props.put("baseObject.lastTimestampMicros", item.getBaseObject().getLastTimestampMicros() !=null ? item.getBaseObject().getLastTimestampMicros() : "null");
			  props.put("baseObject.srcAddress", item.getSrcAddress().toString());
			  props.put("baseObject.srcPort", item.getSrcPort());
			  props.put("baseObject.destAddress", item.getDestAddress().toString());
			  props.put("baseObject.destPort", item.getDestPort());
			  props.put("uuid",new CUUID(item.getUuid()).getHexUUID());
			  vertex = Long.valueOf(bi.createNode(props,MarpleLabels.NetflowObject));
			  vertexMap.put(cuuid,vertex);
			  rVertexMap.put(vertex, cuuid);
		  } else {
			  RuntimeException re = new RuntimeException("WARNING: duplicate NetFlowObject UUID: "+cuuid.getUUID().toString());
			  re.printStackTrace();				  			  
		  }
	  }
	  
	  /**
	   * Extract the CDM properties for the SimpleEdge object and create Labeled node
	   * of type "EDGE_XXX".   @see {@link #edgeMap}
	   * 
	   * @param item - The SimpleEdge to be stored as vertex in the graph
	   * @param props - The properties object to store the SimpleEdge attributes into.
	   */
		private void processSimpleEdge(SimpleEdge item, HashMap<String, Object> props) {

			  Long fromVertex = vertexMap.get(new CUUID(item.getFromUuid()));
			  Long toVertex = vertexMap.get(new CUUID(item.getToUuid()));
			  // TODO: index by timestamp??
			  props.put("timestamp", item.getTimestamp());
			  if (fromVertex == null )
			  {
				  //throw new RuntimeException("item.getFromUuid() not found in vertexMap!");
				  System.err.println("From vertex not found in vertexMap!  Dropping edge.");
				  return;
			  }
			  if (toVertex == null)
			  {
				  System.err.println("To vertex not found in vertexMap!  Dropping edge.");
				  return;
			  }
			  long relId =	bi.createRelationship( fromVertex,toVertex,edgeMap.get(item.getType()) ,props);
			  
			  // subject -> event's parms
			  Long eventVertex = fromVertex;
			  Long subjectVertex = toVertex;
			  if (this.eventVertex2Parms.get(eventVertex) != null)
			  {
				  for (Long parmVertex : this.eventVertex2Parms.get(eventVertex))
				  {
					  bi.createRelationship(parmVertex, subjectVertex, MarpleRelationships.EDGE_OBJECT_ISGENERATEDBY_SUBJECT, new HashMap<String, Object>() );
				  }
			  }
			  
		}

	  /**
	   * Extract the CDM properties for the FileObject object and create Labeled node
	   * of type "FileObject".   
	   * 
	   * @param item - The FileObject to be stored as vertex in the graph
	   * @param props - The properties object to store the FileObject attributes into.
	   */
      private void processFileObject(FileObject item, HashMap<String,Object> props)
      {
    	  CUUID cuuid = new CUUID(item.getUuid(),MarpleLabels.FileObject.toString());
		  Long vertex = vertexMap.get(cuuid);
		  if (vertex==null)
		  {
			  props.put("baseObject.source", item.getBaseObject().getSource().name());
			  SHORT p = item.getBaseObject().getPermission();
			  if (Arrays.asList(p) != null)
			  {
				  saveShorts(Arrays.asList(p),props,"baseObject.permission");
			  } else  {
				  props.put("baseObject.permission",item.getBaseObject().getPermission() !=null ? item.getBaseObject().getPermission()  : "null");
			  }
			  Integer version = item.getVersion();
			  props.put("baseObject.lastTimestampMicros."+version, item.getBaseObject().getLastTimestampMicros() !=null ? item.getBaseObject().getLastTimestampMicros() : "null");
			  props.put("url",item.getUrl().toString());
			  props.put("isPipe",item.getIsPipe());
			  props.put("version", item.getVersion());
			  props.put("size", item.getSize()!=null ?item.getSize() : -1L);
			  props.put("uuid",new CUUID(item.getUuid()).getHexUUID());
			  vertex = Long.valueOf(bi.createNode(props,MarpleLabels.FileObject));
			  if (vertex==-1)
			  {
				  throw new RuntimeException("Error creating vertex.  Props:"+props.toString());
			  }
			  vertexMap.put(cuuid,vertex);	
			  rVertexMap.put(vertex, cuuid);
		  } else {
			  CUUID verifyCuuid = rVertexMap.get(vertex);
			  if (verifyCuuid.getType().compareTo(MarpleLabels.FileObject.toString())!=0)
			  {
				  RuntimeException re = new RuntimeException("DATA ERROR!: Origional datatype of UUID: "+verifyCuuid.getHexUUID()+" was: "+verifyCuuid.getType() +" new type is: "+cuuid.getType() );
				  re.printStackTrace();
				  return;
			  }
			  // This will be expensive.
			  Map<String,Object> pMap = bi.getNodeProperties(vertex);
			  Integer version = item.getVersion();
			  pMap.put("baseObject.lastTimestampMicros."+version, item.getBaseObject().getLastTimestampMicros() !=null ? item.getBaseObject().getLastTimestampMicros() : "null");
			  pMap.put("version",version );
			  bi.setNodeProperties(vertex, pMap);
			  if (debug)
			  {
				  RuntimeException re = new RuntimeException("WARNING: duplicate FileOject UUID: "+cuuid.getUUID().toString()+"  Updating version, and timestmap property from current FileObject");
				  re.printStackTrace();				  			  
			  }
		  }
      }
      /*
       * Extract the CDM properties for the SrcSinkObject object and create Labeled node
	   * of type "SrcSinkObject".  
	   * 
	   * @param item - The SrcSinkObject to be stored as vertex in the graph
	   * @param props - The properties object to store the SrcSinkObject attributes into.
	   * 
       */
      private void processSrcSinkObject(SrcSinkObject item,HashMap<String,Object> props )
      {
    	  CUUID cuuid = new CUUID(item.getUuid(),MarpleLabels.SrcSinkObject.toString());
		  Long vertex = vertexMap.get(cuuid);
		  if (vertex==null)
		  {
			  props.put("baseObject.source", item.getBaseObject().getSource().name());
			  SHORT p = item.getBaseObject().getPermission();
			  if (Arrays.asList(p) != null)
			  {
				  saveShorts(Arrays.asList(p),props,"baseObject.permission");
			  } else  {
				  props.put("baseObject.permission",item.getBaseObject().getPermission() !=null ? item.getBaseObject().getPermission()  : "null");
			  }
			  props.put("baseObject.lastTimestampMicros", item.getBaseObject().getLastTimestampMicros() !=null ? item.getBaseObject().getLastTimestampMicros() : "null");
			  props.put("type", item.getType().toString());
			  storeVertexItemProps(item.getBaseObject().getProperties(),props);
			  props.put("uuid",new CUUID(item.getUuid()).getHexUUID());
			  vertex = Long.valueOf(bi.createNode(props,MarpleLabels.SrcSinkObject));
			  vertexMap.put(cuuid,vertex);	
		  } else {
			  RuntimeException re = new RuntimeException("WARNING: duplicate SrcSinkOject UUID: "+cuuid.getUUID().toString());
			  re.printStackTrace();				  			  
		  }

      }
      
      /**
       * Extract the CDM properties for the MemoryObject object and create Labeled node
	   * of type "MemoryObject".  
	   * 
	   * @param item - The MemoryObject to be stored as vertex in the graph
	   * @param props - The properties object to store the MemoryObject attributes into.
       */
      private void processMemoryObject(MemoryObject item,HashMap<String,Object> props)
      {
			 CUUID cuuid = new CUUID(item.getUuid(),MarpleLabels.MemoryObject.toString());
			 Long vertex = vertexMap.get(cuuid);
			 if (vertex==null)
			 {
				  props.put("uuid",cuuid.getHexUUID());
				  props.put("pageNum",item.getPageNumber()!=null ? item.getPageNumber() : -1L);
				  props.put("address", item.getMemoryAddress());
				  props.put("baseObject.source", item.getBaseObject().getSource().name());
				  SHORT p = item.getBaseObject().getPermission();
				  if (Arrays.asList(p) != null)
				  {
					  saveShorts(Arrays.asList(p),props,"baseObject.permission");
				  } else  {
					  props.put("baseObject.permission",item.getBaseObject().getPermission() !=null ? item.getBaseObject().getPermission()  : "null");
				  }
				  props.put("baseObject.lastTimestampMicros", item.getBaseObject().getLastTimestampMicros() !=null ? item.getBaseObject().getLastTimestampMicros() : "null");
				  storeVertexItemProps(item.getBaseObject().getProperties(),props);
				  vertex = Long.valueOf(bi.createNode(props,MarpleLabels.MemoryObject));
				  vertexMap.put(cuuid,vertex);	

			 } else {
				  RuntimeException re = new RuntimeException("WARNING: duplicate MemoryOject UUID: "+cuuid.getUUID().toString());
				  re.printStackTrace();				  			  				 
			 }
      }
      
      /**
       * Extract the CDM properties for the TagEntity object and create Labeled node
	   * of type "TagEntity".  
	   * 
	   * @param item - The TagEntity to be stored as vertex in the graph
	   * @param props - The properties object to store the TagEntity attributes into.
       */
      private void processTagEntity(TagEntity item, HashMap<String,Object> props)
      {
		  CUUID cuuid = new CUUID(item.getUuid(),MarpleLabels.TagEntity.toString());
		  Long vertex = vertexMap.get(cuuid);
		  if (vertex==null)
		  {
			  HashMap<String,Object> ptnProps = new HashMap<String,Object>();
			  CUUID ptmCuuid = storeProvenanceTagNode(ptnProps, item.getTag());
			  props.put("uuid",cuuid.getHexUUID());
			  props.put("tagCuuid", ptmCuuid.getHexUUID());
			  storeVertexItemProps(item.getProperties(),props);
			  vertex = Long.valueOf(bi.createNode(props,MarpleLabels.TagEntity));
			  vertexMap.put(cuuid,vertex);	
			  Long toVertex = vertexMap.get(ptmCuuid);
			  long relId =	bi.createRelationship( vertex,toVertex,MarpleRelationships.EDGE_EVENT_HAS_TAG ,new HashMap<String,Object>());			  
		  }  else {
			  RuntimeException re = new RuntimeException("WARNING: duplicate TagEntity UUID: "+cuuid.getUUID().toString());
			  re.printStackTrace();				  			  				 
		 }

      }
      
      
    /**
     * 
     * 
     * 
     * @param item
     * @param props
     */
  	private void processRegistryKeyObject(RegistryKeyObject item, HashMap<String, Object> props) {
		CUUID cuuid = new CUUID(item.getUuid(),MarpleLabels.RegistryKeyObject.toString());
		  Long vertex = vertexMap.get(cuuid);
		  if (vertex==null)
		  {
			  props.put("version",item.getVersion());
			  props.put("baseObject.source", item.getBaseObject().getSource().name());
			  SHORT p = item.getBaseObject().getPermission();
			  if (Arrays.asList(p) != null)
			  {
				  saveShorts(Arrays.asList(p),props,"baseObject.permission");
			  } else  {
				  props.put("baseObject.permission",item.getBaseObject().getPermission() !=null ? item.getBaseObject().getPermission()  : "null");
			  }
			  props.put("baseObject.lastTimestampMicros", item.getBaseObject().getLastTimestampMicros() !=null ? item.getBaseObject().getLastTimestampMicros() : "null");
			  props.put("type", MarpleLabels.RegistryKeyObject.toString());
			  storeVertexItemProps(item.getBaseObject().getProperties(),props);
			  props.put("uuid",cuuid.getHexUUID());
			  vertex = Long.valueOf(bi.createNode(props,MarpleLabels.RegistryKeyObject));
			  vertexMap.put(cuuid,vertex);	
		  }	else {
			  if (debug)
			  {
			    RuntimeException re = new RuntimeException("WARNING: duplicate RegistryKeyobject: "+cuuid.getHexUUID().toString());
			    re.printStackTrace();
			  }
			  Map<String,Object> pMap = bi.getNodeProperties(vertex);
			  Integer version = item.getVersion();
			  pMap.put("baseObject.lastTimestampMicros."+version, item.getBaseObject().getLastTimestampMicros() !=null ? item.getBaseObject().getLastTimestampMicros() : "null");
			  pMap.put("version",version );
			  bi.setNodeProperties(vertex, pMap);			  
		  }
	}

  	private void store(Object datum)
  	{
		  HashMap<String,Object> props = new HashMap<String,Object>();

		  System.out.println("dataum is an instanceof: "+datum.getClass().getName());
		  
		  switch (datum.getClass().getName()) {
		  case "com.bbn.tc.schema.avro.Principal":
		  {
			  processPrincipal((Principal) datum,props);
		  }
		  break;
		  case "com.bbn.tc.schema.avro.Subject":
		  {
			  processSubject((Subject)datum,props);

		  }
		  break;
		  case "com.bbn.tc.schema.avro.Event":
		  {
			  Event item = (com.bbn.tc.schema.avro.Event)datum;
			  processEvent((com.bbn.tc.schema.avro.Event)datum,props);
		  }
		  break;
		  case "com.bbn.tc.schema.avro.NetFlowObject":
		  {
			  NetFlowObject item = (com.bbn.tc.schema.avro.NetFlowObject)datum;
			  processNetFlowObject(item,props);
		  }
		  break;
		  case "com.bbn.tc.schema.avro.SimpleEdge":
		  {
			  SimpleEdge item = (com.bbn.tc.schema.avro.SimpleEdge)datum;
			  processSimpleEdge(item,props);

		  }
		  break;
		  case "com.bbn.tc.schema.avro.FileObject":
		  {
			  FileObject item = (com.bbn.tc.schema.avro.FileObject)datum;
			  processFileObject((FileObject)datum,props);

		  }
		  break;
		  case "com.bbn.tc.schema.avro.SrcSinkObject":
		  {
			  SrcSinkObject item = (SrcSinkObject)datum;
			  processSrcSinkObject(item,props);
		  }
		  break;
		  case "com.bbn.tc.schema.avro.ProvenanceTagNode":
		  {
			  CUUID wdc = storeProvenanceTagNode(props, datum);
			  
		  }
		  break;
		  case "com.bbn.tc.schema.avro.MemoryObject":
		  {
			 MemoryObject item = (MemoryObject)datum;
			 processMemoryObject(item,props);
		  }
		  break;
		  case "com.bbn.tc.schema.avro.TagEntity":
		  {
			  TagEntity item = (TagEntity)datum;
			  processTagEntity(item,props);
		  }
		  break;
		  case "com.bbn.tc.schema.avro.RegistryKeyObject":
		  {
			  RegistryKeyObject item = (RegistryKeyObject)datum;
			  processRegistryKeyObject(item,props);
		  }
		  break;
		  default:
		  {
			  System.err.println("WARNING: Object instance type: "+datum.getClass().getName()+" not processed.");
		  }
		  }  	
  	}
      
	  public void store(TCCDMDatum record)
	  {
		  Object datum = record.getDatum();
		  store(datum);

	  }


   private UUID getUUID()
   {
		  byte  highBytes[] =String.valueOf(System.currentTimeMillis()).getBytes();
		  UUID uuid = new UUID(highBytes);
		  return uuid;
   }


	/**
	 * @param props
	 * @param datum
	 */
	private CUUID storeProvenanceTagNode(HashMap<String, Object> props, Object datum) {
		ProvenanceTagNode item = (ProvenanceTagNode)datum;
		  // NOTE This is a UUID generated from the tag id, not a supplied UUID.
		  // I have seen inconsistent data type for the "value" field of a ProvenanceTagNode.  Sometimes, it's an string "TAG_OP_XXX" other times it's a UUID array.  What gives? 
		  //UUID tmp = new UUID(String.valueOf(item.getTagId()).getBytes());
		  Object blob = item.getValue();
		  UUID uuid = null;
		  CUUID ptnCuuid=null;
		  UUID ptnUuid=null;
		  if (blob instanceof UUID)
		  {
			  uuid = (UUID)item.getValue();
			  
		  } else if (blob instanceof Integer)
		  {
			  uuid = new UUID(String.valueOf(item.getValue()).getBytes());  
		  } else if (blob instanceof TagOpCode)
		  {
			  System.err.println("ERROR: TagOpCode as the only value is not globally unique.");
			  TagOpCode opCode = (TagOpCode)item.getValue();
			  uuid = getUUID();

		  } else if (blob instanceof IntegrityTag)
		  {
			  System.err.println("ERROR: IntegrityTag as the only value is not globally unique.");
			  IntegrityTag it = (IntegrityTag)blob;
			  uuid = getUUID();

		  } else if (blob instanceof ConfidentialityTag)
		  {
			  System.err.println("ERROR: ProvenanceTagNode: ConfidentialityTag as the only value is not globally unique.");
			  ConfidentialityTag ct = (ConfidentialityTag)item.getValue();
			  uuid = getUUID();

		  } else 
		  {
			  System.err.println("ERROR: unknown object in ProvenanceTagNode");
			  uuid = getUUID();
		  }
	
		  CUUID cuuid = new CUUID(uuid,MarpleLabels.ProvenanceTagNode.toString());
		  Long vertex = vertexMap.get(cuuid);
		  if (vertex!=null)
		  {
			  CUUID verifyCuuid = rVertexMap.get(vertex);
			  if (verifyCuuid == null)
			  {
				  RuntimeException re = new RuntimeException("DEBUG check.  verifyCuuid not found in rVertexMap.   uuid="+uuid+" cuuid: "+cuuid.getHexUUID());
				  re.printStackTrace();
				  return cuuid;
			  }
			  if (verifyCuuid.getType().compareTo(cuuid.getType())!=0)
			  {
//				  RuntimeException re = new RuntimeException("DATA ERROR!: Origional datatype of UUID: "+verifyCuuid.getHexUUID()+" was: "+verifyCuuid.getType() +" new type is: "+cuuid.getType() );
//				  re.printStackTrace();
				  // This is a pointer (reference) to a vertex in the graph.  Synthesize a node for this provenanceTagNode, and link it to the verifyCuuid vertex.
				  ptnUuid = getUUID();
				  ptnCuuid = new CUUID(ptnUuid,MarpleLabels.ProvenanceTagNode.toString());
			  } else {
				  // Merge existing properties when a duplicate is encountered.
				  props = (HashMap<String, Object>) bi.getNodeProperties(vertex);
			  }
		  }
		  props.put("uuid",ptnCuuid == null ? cuuid.getHexUUID() : ptnCuuid.getHexUUID());
		  props.put("tagId", item.getTagId()!=null ? item.getTagId() : -1 );
		  Object itemValue = item.getValue();
		  if (itemValue instanceof String) {
			  props.put("value",(String)itemValue);
		  } else if (itemValue instanceof UUID)
		  {
			  props.put("value", new CUUID((UUID)itemValue).getHexUUID());
		  } else if (itemValue instanceof int[])
		  {
			  props.put("value", Arrays.toString((int[])itemValue));
		  } else if (itemValue instanceof long[])
		  {
			  props.put("value", Arrays.toString((long[])itemValue));
		  }
 		  List<ProvenanceTagNode> children = item.getChildren();
		  storeVertexItemProps(item.getProperties(),props);
		  if (vertex==null)
		  {
			  vertex = Long.valueOf(bi.createNode(props,MarpleLabels.ProvenanceTagNode));
			  vertexMap.put( cuuid ,vertex);
			  if (children!=null)
			  {
				  for (ProvenanceTagNode child: children)
				  {
					  HashMap<String, Object> cProps = new HashMap<String,Object>();
					  CUUID cNode = storeProvenanceTagNode(cProps,child);
					  Long toVertex = vertexMap.get(cNode);
					  Long fromVertex = vertex;
					  // TODO: create a relationship from the parent node to the child node.
					  long relId =	bi.createRelationship( fromVertex,toVertex,MarpleRelationships.EDGE_EVENT_HAS_TAG ,new HashMap<String,Object>());	
				  }
			  }
		  } else 
		  {
			  log.info("WARNING:  possible duplicate provananceTagNode: "+item.getTagId());
			  /**
			   * The pointer reference case.  Create a reference between the new vertex, and the old one.
			   */
			  if (ptnUuid!=null)
			  {
				  Long ptnVertex = Long.valueOf(bi.createNode(props,MarpleLabels.ProvenanceTagNode));
				  vertexMap.put(ptnCuuid, ptnVertex);
				  long relId =	bi.createRelationship( ptnVertex,vertex,MarpleRelationships.EDGE_EVENT_HAS_TAG ,new HashMap<String,Object>());					  
			  }
			  
		  }
		  return ptnCuuid==null ? cuuid : ptnCuuid;
	}
	  
	  void shutdown()
	  {
		  bi.shutdown();
	  }

	  
}


class IpPortObject{
	private String ip;
	private String port;
	
	public IpPortObject(String ip, String port){
		this.ip = ip;
		this.port = port;
	}
	
	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
	    if (o == null || getClass() != o.getClass()) return false;
	    return port.equals(((IpPortObject)o).port) && ip.equals(((IpPortObject)o).ip);
	}

	 @Override
	 public int hashCode() {
		 return (ip + port).hashCode();
	 }
	
}
