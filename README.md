This program consumes AVRO CDM and produces Neo4J graphs.
To run the program, export it from Eclipse as a runnable JAR file named,
'Neo4JCdmConsumer.jar'.

The directory structure required to run this program is:
<code>
├── Neo4JCdmConsumer.jar
├── cleanup
├── conf
│   ├── BatchImporter.properties
│   ├── Deserializer.properties
├── data
│   ├── avroFile.avro
│   └── spygate_remotedesktop.avro
├── demo
├── neo4j
└── schema
    ├── TCCDMDatum.avsc
</code>

The neo4j folder should contain the neo4j 3.0.0 community edition release from
http://neo4j.com

A sample program, "demo" to execute the JAR file and build a graph on Mac OS/X:
```
#!/bin/bash
java -Xms1G -Xmx2G -jar Neo4JCdmConsumer.jar
# if reading from kafka:
# java -Xms1G -Xmx2G -jar Neo4JCdmConsumer.jar -topic test-1-1
cd neo4j/bin
open "http://localhost:7474"
./neo4j console
```
A sample program to cleanup the graph database after a failed run, or to re-import
the data:

<code>
#!/bin/bash
cd neo4j/data/databases
rm -rf graph.db
</code>

Sample Deserializer.properties file:

```
# Specify the location of the AVRO CDM input file.
#cdmfile=data/avroFile.avro
cdmfile=data/spygate_remotedesktop.avro
bootstrap.servers=192.168.87.2:9092
exiton0=True
```

